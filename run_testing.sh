# You should run flake before building container,
# the last thing you want is to build a testing
# image and see trailing space on line 42
# For this to work you have to create virtualenv in ".venv"
# and install "test-requirements.txt"
(source .venv/bin/activate && flake8 && mypy -p server && mypy -p tests)
# Docker 18.06+ feature to avoid building unrelated "main" during testing
# Building test image
DOCKER_BUILDKIT=1 docker build -t humanbios-server-test --target=test .
# Running test image
docker run --rm \
    -v /var/run/docker.sock:/var/run/docker.sock \
    --network=caddynet \
    --env-file=.env.test \
    humanbios-server-test
