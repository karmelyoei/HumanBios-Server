
# Deployment
First clone ai repos:
```
git clone git@gitlab.com:humanbios/rasa.git
git clone git@gitlab.com:humanbios/gpt2bot.git
# or via https
git clone https://gitlab.com/humanbios/rasa.git
git clone https://gitlab.com/humanbios/gpt2bot.git
```
Start containers:
```
docker-compose up -d --build
```
