# Deploy
### Configuration
Create `.env` file from sample -  
```
cp .env.sample .env
```
and fill `.env` file.  
Then use `Caddyfile.sample`, change domain address if needed:
```
cp Caddyfile.sample Caddyfile
```
### Start
```
docker-compose up -d
```
