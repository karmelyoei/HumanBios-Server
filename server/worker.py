from .handler import Handler
import threading
import asyncio


class Worker(threading.Thread):
    def __init__(self):
        self.handler = Handler()
        self.keep_running = True
        super().__init__(daemon=True)

    def process(self, ctx):
        asyncio.run_coroutine_threadsafe(self.handler.process(ctx), self._loop)

    async def _run_processes(self):
        self._loop = asyncio.get_event_loop()

        self._tasks = asyncio.ensure_future(asyncio.gather(
            self.handler.reminder_loop(),
            self.handler.broadcast_loop(),
        ))
        await self._tasks

    def run(self):
        asyncio.run(self._run_processes())

    def stop(self):
        self._tasks.cancel()

    def reload_file(self):
        asyncio.run_coroutine_threadsafe(self.handler.task_load_bots_file(), self._loop)
