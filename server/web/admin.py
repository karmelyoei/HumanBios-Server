from jinja2 import Environment, FileSystemLoader, select_autoescape
from sanic.response import json, redirect, stream
from boto3.dynamodb.conditions import Attr
from ..settings import OWNER_HASH, DEBUG
from accept_types import get_best_match
from ..db import PermissionLevel
from sanic import Blueprint
# import collections
import functools
# import iso639


def _require_permission(level: PermissionLevel, allow_unvalidated=False):
    """
    Enforces that the user has the [level] permission bit set.
    If [level] is DEFAULT, enforces that the user has at least one permisson bit set.
    If [level] is None, does not enforce the permissions, but still sets the request.ctx
    """

    def inner(func):

        @_handle_mime
        @functools.wraps(func)
        async def runtime(self, request, *args, json_resp, **kwargs):
            request.ctx.session_id = request.cookies.get("session", None)
            if request.ctx.session_id is None:
                request.ctx.identity = None
                request.ctx.user = None
                request.ctx.permissons = PermissionLevel.DEFAULT
            else:
                request.ctx.validated, request.ctx.identity = await self.database.check_websession(request.ctx.session_id)
                if not request.ctx.validated and not allow_unvalidated:
                    request.ctx.identity = None
                    return self.redirect("/creds")
                if request.ctx.identity:
                    request.ctx.user = await self.database.get_user(request.ctx.identity)
                    if request.ctx.identity == OWNER_HASH:
                        request.ctx.permissions = PermissionLevel.MAX
                    else:
                        request.ctx.permissions = request.ctx.user["permissions"]
                        if request.ctx.permissions > PermissionLevel.MAX:
                            request.ctx.permissions = PermissionLevel.DEFAULT
                else:
                    request.ctx.user = None
                    request.ctx.permissions = PermissionLevel.DEFAULT

            if level is not None:
                if not request.ctx.identity:
                    if json_resp:
                        return json({
                            "status": 401,
                            "message": "authentication required"
                        })
                    else:
                        return self.jinja2("401.html", status=401)
                if not request.ctx.permissions or request.ctx.permissions & level != level:
                    if json_resp:
                        return json({
                            "status": 403,
                            "message": "permission denied",
                            "required_permissions": level
                        })
                    else:
                        return self.jinja2("403.html", {"required_permissions": level}, status=403)
            return await func(self, request, *args, json_resp=json_resp, **kwargs)
        return runtime
    return inner


def _handle_mime(func):

    @functools.wraps(func)
    async def runtime(self, request, *args, **kwargs):
        format = get_best_match(request.headers["Accept"], ("text/html", "text/json"))
        if format is None:
            return json({"status": 400, "message": "invalid 'Accept' header"})
        request.ctx.resp_content_type = format + "; charset=utf-8"
        return await func(self, request, *args, json_resp=(format == "text/json"), **kwargs)
    return runtime


class AdminSite:
    def __init__(self, database, static_url, strings):
        self.database = database
        self.strings = strings
        self.base_url = "/admin"
        self.static_url = static_url
        self.bp = Blueprint("admin", url_prefix=self.base_url)

        self.env = Environment(
            loader=FileSystemLoader("server/web/templates"),
            autoescape=select_autoescape(["html", "xml"]),
            auto_reload=DEBUG,
            enable_async=True,
            trim_blocks=True,
            lstrip_blocks=True,
        )

        self.bp.middleware(self.prevent_xss, "response")
        self.bp.middleware(self.content_type, "response")
        self._add_route(self.get_root, "")
        self._add_route(self.get_auth_token, "/auth/<token>")
        self._add_route(self.get_auth_creds, "/creds")
        self._add_route(self.post_auth_creds, "/creds", methods=("POST",))
        self._add_route(self.get_edit_creds, "/edit_creds")
        self._add_route(self.post_edit_creds, "/edit_creds", methods=("POST",))
        self._add_route(self.get_permissions, "/permissions")
        self._add_route(self.post_permissions, "/permissions", methods=("POST",))
        # self._add_route(self.get_translations, "/translations")
        # self._add_route(self.post_translations, "/translations", methods=("POST",))

    def _add_route(self, handler, *args, **kwargs):
        # AAAAAAAAAAAAAAA
        # TODO: stop using sanic
        self.bp.add_route(functools.wraps(handler)(functools.partial(handler)), *args, **kwargs)

    # middlewares & utils

    def redirect(self, rel_path, *args, **kwargs):
        return redirect(self.base_url + rel_path, *args, **kwargs)

    def error(self, status, return_to, message, json_resp):
        if json_resp:
            return json({"status": status, "message": message})
        else:
            return self.jinja2(
                "error.html",
                {
                    "return_to": self.base_url + return_to,
                    "message": message,
                    "code": status
                },
                status=status
            )

    def success(self, return_to, json_resp):
        if json_resp:
            return json({"status": 200})
        else:
            return self.jinja2("success.html", {"return_to": self.base_url + return_to})

    def jinja2(self, template_name, ctx={}, content_type="text/html; charset=utf-8", **kwargs):  # noqa: B006
        async def render_stream(response):
            ctx.setdefault("base", self.base_url)
            ctx.setdefault("static", self.static_url)
            stream_data = self.env.get_template(template_name).generate_async(**ctx)
            async for chunk in stream_data:
                await response.write(chunk)
        return stream(render_stream, content_type=content_type, **kwargs)

    @staticmethod
    async def prevent_xss(request, response):
        response.headers["x-xss-protection"] = "1; mode=block"

    @staticmethod
    async def content_type(request, response):
        if request.ctx.resp_content_type:
            response.headers["Content-Type"] = request.ctx.resp_content_type

    # admin pages

    @_require_permission(PermissionLevel.DEFAULT)
    async def get_root(self, request, json_resp):
        # TODO: json support
        if request.ctx.permissions > PermissionLevel.DEFAULT:
            return self.jinja2("admin/index.html", {"owner": (request.ctx.permissions >= PermissionLevel.ADMIN)})
        else:
            return self.jinja2("admin/login.html")

    @_require_permission(None, allow_unvalidated=True)
    async def get_auth_token(self, request, token, json_resp):
        if request.ctx.identity:
            if json_resp:
                return json({"status": 204})
            else:
                return self.redirect("/")
        identity = await self.database.check_webtoken(token)
        if not identity:
            return self.error(401, False, "Login token incorrect", json_resp)
        session = await self.database.create_websession(identity)
        response = self.redirect("/creds")
        response.cookies["session"] = session
        response.cookies["session"]["max-age"] = 24 * 60 * 60
        response.cookies["session"]["secure"] = not DEBUG
        response.cookies["session"]["httponly"] = True
        return response

    @_require_permission(PermissionLevel.DEFAULT, allow_unvalidated=True)
    async def get_auth_creds(self, request, json_resp):
        if request.ctx.validated:
            if json_resp:
                return json({"status": 204})
            else:
                return self.redirect("/")
        return self.jinja2("admin/creds.html")

    @_require_permission(PermissionLevel.DEFAULT, allow_unvalidated=True)
    async def post_auth_creds(self, request, json_resp):
        if request.ctx.validated:
            if json_resp:
                return json({"status": 204})
            else:
                return self.redirect("/")
        password = request.form.get("password", "").encode("utf-8")
        if len(password) not in range(8, 32):
            return self.error(
                400,
                "/creds",
                "password length out of range "
                "(must be at least 8 characters and shorter than 32 characters)",
                json_resp
            )
        if await self.database.check_webcredentials(request.ctx.identity, password):
            await self.database.set_websession_status(request.ctx.session_id, True)
            return self.success("/", json_resp)
        else:
            return self.error(401, "/creds", "Password incorrect", json_resp)

    @_require_permission(PermissionLevel.DEFAULT)
    async def get_edit_creds(self, request, json_resp):
        return self.jinja2("admin/edit_creds.html")

    @_require_permission(PermissionLevel.DEFAULT)
    async def post_edit_creds(self, request, json_resp):
        current_password = request.form.get("current_password", "").encode("utf-8")
        new_password = request.form.get("new_password", "").encode("utf-8")
        if len(current_password) not in range(8, 32):
            return self.error(
                400,
                "/edit_creds",
                "current password length out of range "
                "(must be at least 8 characters and shorter than 32 characters)",
                json_resp
            )
        if len(new_password) not in range(8, 32):
            return self.error(
                400,
                "/edit_creds",
                "new password length out of range "
                "(must be at least 8 characters and shorter than 32 characters)",
                json_resp
            )
        if current_password == new_password:
            return self.error(400, "/edit_creds", "new password is unchanged", json_resp)
        if await self.database.set_webcredentials(request.ctx.identity, current_password, new_password):
            return self.success("/edit_creds", json_resp)
        else:
            return self.error(401, "/edit_creds", "current password incorrect", json_resp)

    @_require_permission(PermissionLevel.ADMIN)
    async def get_permissions(self, request, json_resp):
        admins = list()
        async for admin in self.database.scan_users(
            ~ Attr("permissions").eq(PermissionLevel.DEFAULT),
            "#idtt, permissions",
            {"#idtt": "identity"}
        ):
            admins.append(admin)
        return self.jinja2("admin/permissions.html", {
            "data": admins,
            "PermissionLevel": lambda l: ", ".join(PermissionLevel(lvl).name.lower()
                                                   for lvl in PermissionLevel if lvl & int(l)),
            "Level": PermissionLevel
        })

    @_require_permission(PermissionLevel.ADMIN)
    async def post_permissions(self, request, json_resp):
        identity = request.form.get("identity", "")
        if len(identity) != 64:
            return self.error(400, "/permissions", "invalid identity", json_resp)
        level = 0
        for key in request.form:
            if key.startswith("level_"):
                try:
                    key = int(key[6:])
                except ValueError:
                    return self.error(400, "/permissions", f"invalid level {key}", json_resp)
                level |= key
        if level >= PermissionLevel.MAX or level < 0:
            return self.error(400, "/permissions", f"invalid level {level}", json_resp)
        await self.database.update_user(
            identity,
            "SET #permissions = :v",
            {"#permissions": "permissions"},
            {":v": level},
        )
        return self.success("/permissions", json_resp)

    """
    @_require_permission(PermissionLevel.TRANSLATOR)
    async def get_translations(self, request, json_resp):
        translations = collections.defaultdict(dict)
        async for translation in self.database.iter_all_translation():
            if translation.get("manual", False):
                translations[translation["language"]][translation["string_key"]] = translation["text"]
        ctx = {
            "strings": translations,
            "languages": {lang["iso639_1"]: lang["name"] for lang in iso639.data if lang["iso639_1"]},
            "keys": self.strings.original_strings.keys()
        }

        if json_resp:
            return json({"status": 200, **ctx})
        else:
            return self.jinja2("admin/translations.html", ctx)

    @_require_permission(PermissionLevel.TRANSLATOR)
    async def post_translations(self, request, json_resp):
        lang = request.form.get("language")
        if not lang or lang == "en":
            return self.error(400, "/translations", "invalid language", json_resp)
        key = request.form.get("key")
        if not key:
            return self.error(400, "/translations", "invalid key", json_resp)
        text = request.form.get("value")
        if not text:
            return self.error(400, "/translations", "invalid text", json_resp)
        old = self.strings.original_strings.get(key)
        if not old:
            return self.error(400, "/translations", "unknown key", json_resp)
        hash = old["hash"]
        self.strings.cache.setdefault(lang, {})[key] = {"text": text, "hash": hash}
        await self.database.create_translation({
            "language": lang,
            "string_key": key,
            "content_hash": hash,
            "text": text,
            "manual": True
        })
        return self.success("/translations", json_resp)
    """
