from ..settings import CLOUD_TRANSLATION_API_KEY
from aiohttp import ClientSession
from typing import Optional
import logging
import asyncio
import ujson


class Translator:
    TRANSLATION_URL = f"https://translation.googleapis.com/language/translate/v2?key={CLOUD_TRANSLATION_API_KEY}"
    DETECTION_URL = f"https://translation.googleapis.com/language/translate/v2/detect?key={CLOUD_TRANSLATION_API_KEY}"
    HEADERS = {
        "Content-Type": "application/json; charset=utf-8",
    }

    # @Thought: Maybe drop __init__ func and move key to the class var
    # @Thought: then change all methods to @classmethod
    # @Thought: No. But we should make key a constructor parameter.
    def __init__(self):
        self.key = CLOUD_TRANSLATION_API_KEY

    async def __get_json(self, url: str, data: dict, headers: dict, session: Optional[ClientSession]):
        if session:
            async with session.post(url, json=data, headers=headers) as response:
                return await response.json()
        else:
            async with ClientSession() as session:
                async with session.post(url, json=data, headers=headers) as response:
                    return await response.json()

    async def detect_language(self, text: str, session: Optional[ClientSession] = None):
        data = {"q": text}
        result = await self.__get_json(self.DETECTION_URL, data, self.HEADERS, session)
        # Schema: [[{LANG}], ...], where LANG: {"language": "en"}
        # logging.info(result)
        return result["data"]["detections"][0][0]["language"]

    async def translate_text(self, text: str, target: str, session: Optional[ClientSession] = None, from_lang: str = "en") -> str:
        if len(self.key) != 39:
            return self.shim_translate_text(text, target, session, from_lang)
        data = {
            "source": from_lang,
            "target": target,
            "format": "text",
            "q": text,
        }
        result = await self.__get_json(
            self.TRANSLATION_URL, data, self.HEADERS, session
        )
        return result["data"]["translations"][0]["translatedText"]

    def shim_translate_text(self, text: str, target: str, session: ClientSession = None, from_lang: str = "en") -> str:
        """
        Return a shim translation, for debugging.
        It is easily identifiable without needing cloud translations
        """
        return f"{text} [{from_lang}->{target}]"

    async def translate_dict(self, target: str, texts: dict, from_lang: str = "en"):
        new_texts = dict()
        async with ClientSession() as session:
            tasks = await asyncio.gather(*[self.translate_text(text, target, session) for text in texts.values()])
            for i, key in enumerate(texts):
                new_text = tasks[i]
                # :< Hacks TODO: fix escaping
                new_text = new_text.replace("& deg;", "°")
                new_texts[key] = new_text
        return new_texts

    async def translate_promises(self, promises, source):
        async with ClientSession() as s:
            tasks = await asyncio.gather(*[self.translate_text(source[p.key]["text"], p.lang, s) for p in promises])
            for i, p in enumerate(promises):
                new_text = tasks[i]
                # :< Hacks TODO: fix escaping
                new_text = new_text.replace("& deg;", "°")
                p.fill(new_text)

    async def translate_to_multiple_languages_dict(self, languages: list, source_dict: dict, from_lang: str = "en"):
        tasks_group = asyncio.gather(
            *[
                self.translate_dict(language, source_dict, from_lang)
                for language in languages
            ]
        )
        res = await tasks_group
        results = dict()
        for i, lang in enumerate(languages):
            results[lang] = res[i]
        return results

    async def translation(self, source_file: str, from_lang: str, languages: list, output_file: str):
        with open(source_file) as f:
            source = ujson.load(f)

        results = await self.translate_to_multiple_languages_dict(languages, source, from_lang)
        result_json = {
            from_lang: source
        }
        result_json.update(results)

        with open(output_file, "w") as output:
            ujson.dump(result_json, output, indent=4)

        logging.info("Translation done!")
