from ..strings import TextPromise
import decimal
import json


class CustomEncoder(json.JSONEncoder):
    def default(self, o):
        if isinstance(o, TextPromise):
            return str(o)
        elif isinstance(o, decimal.Decimal):
            return float(o)
        return json.JSONEncoder.default(self, o)


def custom_default(obj):
    if isinstance(obj, decimal.Decimal):
        return float(obj)
    elif isinstance(obj, TextPromise):
        return str(obj)
    raise TypeError
