from .db import Database, User, BroadcastMessage, Session, PermissionLevel, CheckBack, AccountType
from .definitions import OK, BACK, GO_TO_STATE, END, NOOP
from .settings import tokens, ROOT_PATH, OWNER_HASH
from datetime import timedelta, datetime
from .translation import Translator
from .rasa_lib import NLUWorker
from .strings import Strings
from .states import collect
from typing import List
import inspect
import asyncio
import aiohttp
import logging
import json
import os


class Handler(object):
    STATES_HISTORY_LENGTH = 10
    END_STATE = "ENDState"
    START_STATE = "StartState"
    BLOGGING_STATE = "BloggingState"
    BROADCASTING_STATE = "BroadcastingState"
    GET_ID_STATE = "GetIdState"
    EDIT_PERMISSIONS_STATE = "EditPermissionsState"
    AUTHORIZATION_STATE = "AuthorizationState"
    LANGUAGE_DETECTION_STATE = "LanguageDetectionState"
    LATEST_DATA_PATH = os.path.join(ROOT_PATH, "server", "archive", "latest.json")

    def __init__(self):
        self.__states = {}
        self.__register_states(*collect())
        self.db = Database()
        self.translator = Translator()
        self.nlu = NLUWorker(self.translator)
        self.strings = Strings(self.translator, self.db)
        self.bots_data = None

        if os.path.exists(self.LATEST_DATA_PATH):
            self.load_bots_file()
            logging.info("Succesfuly loaded Core Botsociety file.")
        else:
            logging.warning("No Core Botsociety file was found!")

    def __register_state(self, state_class):
        self.__states[state_class.__name__] = state_class

    def __register_states(self, *states_):
        for state in states_:
            self.__register_state(state)

    def __get_state(self, name):
        if callable(name):
            name = name.__name__
        state = self.__states.get(name)
        if state is None:
            # If non-existing state - send user to the start state
            # @Important: Don't forget to initialize the state
            return (
                False,
                self.__states[Handler.START_STATE](
                    db=self.db,
                    tr=self.translator,
                    nlu=self.nlu,
                    strings=self.strings,
                    bots_data=self.bots_data,
                ),
                Handler.START_STATE
            )
        # @Important: Don't forget to initialize the state
        return (
            True,
            state(
                db=self.db,
                tr=self.translator,
                nlu=self.nlu,
                strings=self.strings,
                bots_data=self.bots_data,
            ),
            name
        )

    async def __get_or_register_user(self, context) -> User:
        # Getting user from database
        user = await self.db.get_user(context["request"]["user"]["identity"])
        if user is None:
            # Special case for the owner to log in, requires an owner identity, and gives maximum permissions..
            if OWNER_HASH == context["request"]["user"]["identity"]:
                permissions = PermissionLevel.MAX
            # ..all other users are assigned lowest permission rights.
            else:
                permissions = PermissionLevel.ANON

            # Creating new user
            user = {
                "user_id":      context["request"]["user"]["user_id"],
                "service":      context["request"]["service_in"],
                "identity":     context["request"]["user"]["identity"],
                "via_instance": context["request"]["via_instance"],
                "first_name":   context["request"]["user"]["first_name"],
                "last_name":    context["request"]["user"]["last_name"],
                "username":     context["request"]["user"]["username"],
                "language":     "en",
                "account_type": AccountType.COMMON,
                "created_at":   self.db.now().isoformat(),
                "location":     None,
                "last_active":  self.db.now().isoformat(),
                "conversation": None,
                "answers":      dict(),
                "files":        dict(),
                "states":       ["ENDState"],
                "permissions":  permissions,
                "context":      dict(),
            }
            await self.db.create_user(user)

        # @Important: Dynamically update associated service instance, when it was changed
        if context["request"]["via_instance"] != user["via_instance"]:
            # Update database
            user = await self.db.update_user(
                user["identity"],
                "SET via_instance = :v",
                None,
                {":v": context["request"]["via_instance"]},
                user=user,
            )

        # await self.__register_event(user)
        return user

    # async def __register_event(self, user: User):
    #    # TODO: REGISTER USER ACTIVITY (Logging usage statistics)
    #    pass

    async def process(self, context):
        # Getting or registering user
        user = await self.__get_or_register_user(context)
        # Finding last registered state of the user
        special_state = await self.get_command_state(user, context)
        # Regular handling for special states.
        if special_state:
            await self.__forward_to_state(context, user, special_state)
            return

        last_state = await self.last_state(user, context)
        # Looking for state, creating state object
        correct_state, current_state, current_state_name = self.__get_state(last_state)
        if not correct_state:
            user["states"].append(current_state_name)
            # @Important: maybe we don't need to commit, since we will commit after?
            # await self.db.commit_user(user)
        # Call process method of some state
        ret_code = await current_state.wrapped_process(context, user)
        await self.__handle_ret_code(context, user, ret_code)

    async def get_command_state(self, user: User, context):
        text = context["request"]["message"]["text"]
        if isinstance(text, str) or hasattr(text, "value"):
            text = str(text)
            if text.startswith("/start"):
                context["request"]["message"]["text"] = text[6:].strip()
                return Handler.START_STATE
            if text.startswith("/postme"):
                return Handler.BLOGGING_STATE
            if text.startswith("/broadcast"):
                return Handler.BROADCASTING_STATE
            if text.startswith("/id"):
                return Handler.GET_ID_STATE
            if text.startswith("/edit_permissions"):
                return Handler.EDIT_PERMISSIONS_STATE
            if text.startswith("/login"):
                return Handler.AUTHORIZATION_STATE
            if text.startswith("/webtoken"):
                return "WebAdminLoginState"
            if text.startswith("/reminder"):
                return "ReminderState"
            if text.startswith("/new_access_token"):
                return "NewAccessTokenState"
            if text.startswith("/tokens"):
                return "AccessTokensMenuState"
            if text.startswith("/language"):
                return Handler.LANGUAGE_DETECTION_STATE

    # get last state of the user
    async def last_state(self, user: User, context):
        # defaults to START_STATE
        try:
            return user["states"][-1]
        except IndexError:
            pass
        except KeyError:
            user["states"] = ["ENDState"]
        return Handler.START_STATE

    def isstatus(self, ret_code, status_class):
        if inspect.isclass(ret_code):
            return ret_code == status_class
        else:
            return isinstance(ret_code, status_class)

    async def __handle_ret_code(self, context, user, ret_code):
        # Handle return codes
        #    OK          -> do nothing, the state is preserved
        #    END         -> go to the END_STATE
        #    BACK        -> go to the previous state
        #    GO_TO_STATE -> proceed executing wanted state
        #    NOOP        -> do nothing, states are reset
        if not ret_code.process_next or self.isstatus(ret_code, OK):
            return

        elif self.isstatus(ret_code, GO_TO_STATE):
            await self.__forward_to_state(
                context, user, ret_code.next_state, entry=ret_code.entry
            )
        elif self.isstatus(ret_code, BACK):
            user["states"].pop()
            await self.__forward_to_state(
                context, user, user["states"][-1], entry=ret_code.entry
            )
        elif self.isstatus(ret_code, END):
            await self.__forward_to_state(
                context, user, self.END_STATE, entry=ret_code.entry
            )
        elif self.isstatus(ret_code, NOOP):
            user["states"] = ["StartState"]
            await self.db.commit_user(user)

    async def __forward_to_state(self, context, user, next_state, entry=False):
        last_state = await self.last_state(user, context)
        correct_state, current_state, current_state_name = self.__get_state(next_state)

        # Here you drop the user, if they don't have good enough permissions.
        # if user["permissions"] < current_state.permission_required:
        #     return

        if current_state_name != last_state:
            # Registering new last state
            user["states"].append(current_state_name)
            # @Important: maybe we don't need to commit, since we will commit after?
            # await self.db.commit_user(user)
            # Check if history is too long
            if len(user["states"]) > self.STATES_HISTORY_LENGTH:
                # @Important: maybe we don't need to update, since we will commit after?
                # await self.db.update_user(user['identity'], "REMOVE states[0]", None, None, user)
                user["states"].pop(0)

        if current_state.has_entry and (current_state_name != last_state or entry):
            ret_code = await current_state.wrapped_entry(context, user)
        else:
            ret_code = await current_state.wrapped_process(context, user)
        await self.__handle_ret_code(context, user, ret_code)

    async def reminder_loop(self) -> None:
        try:
            logging.info("Reminder loop started")
            while True:
                now = self.db.now()
                # next_circle = (now + timedelta(minutes=1)).replace(second=0, microsecond=0)
                next_circle = (now + timedelta(seconds=10)).replace(microsecond=0)
                await asyncio.sleep((next_circle - now).total_seconds())
                await self.schedule_nearby_reminders(next_circle)
        except asyncio.CancelledError:
            logging.info("Reminder loop stopped")
        except Exception as e:
            logging.exception(f"Exception in reminder loop: {e}")

    async def schedule_nearby_reminders(self, now: datetime) -> None:
        # until = now + timedelta(minutes=1)
        until = now + timedelta(seconds=10)
        all_items_in_range, count = await self.db.all_checkbacks_in_range(now, until)
        # Send broadcast in the next minute, but not all at the same time
        send_at_list = [(60 / count) * i for i in range(count)]
        async with aiohttp.ClientSession() as session:
            await asyncio.gather(
                *[
                    self.send_reminder(send_at, checkback, session)
                    for send_at, checkback in zip(send_at_list, all_items_in_range)
                ]
            )

    async def send_reminder(self, send_at: float, checkback: CheckBack, session: aiohttp.ClientSession):
        try:
            logging.debug(f"Sending checkback after {send_at} seconds")
            await asyncio.sleep(send_at)
            logging.debug("Sending checkback")
            await self._send_reminder(checkback, session)
        except Exception as e:
            logging.exception(f"Failed to send reminder: {e}")

    async def _send_reminder(self, reminder: CheckBack, session: aiohttp.ClientSession):
        # If the reminder has state that user must be sent to, apply it here.
        if reminder["state"] != "empty":
            await self.db.update_user(
                reminder["identity"],
                "SET states = list_append(states, :i)",
                None,
                {":i": [reminder["state"]]},
            )
        # If the reminder is 'daily' or 'weekly' we update its new time here accordinly.
        period = reminder["period"]
        if period in ["daily", "weekly"]:
            add_delta = timedelta(days=1 if period == "daily" else 7)
            next_time = datetime.fromisoformat(reminder["send_at"]) + add_delta
            await self.db.update_checkback(
                reminder["id"],
                "SET send_at = :v",
                {":v": next_time.isoformat()}
            )

        context = json.loads(reminder["context"])
        url = tokens[context["via_instance"]].url

        async with session.post(url, json=context) as response:
            # If reached server - log response
            if response.status == 200:
                result = await response.json()
                logging.debug("Sending checkback status: %s", result)
                return result
            # Otherwise - log error
            else:
                text = await response.text()
                logging.error(
                    f"[ERROR]: Sending checkback (send_at={reminder['send_at']}, "
                    f"identity={reminder['identity']}) status {text}"
                )

    async def broadcast_loop(self):
        try:
            logging.info("Broadcast loop started")
            while True:
                now = self.db.now()
                next_circle = (now + timedelta(minutes=1)).replace(second=0, microsecond=0)
                await asyncio.sleep((next_circle - now).total_seconds())
                await self.schedule_broadcasts()
        except asyncio.CancelledError:
            logging.info("Broadcasts loop stopped")
        except Exception as e:
            logging.exception(f"Exception in broadcasts loop: {e}")

    async def schedule_broadcasts(self):
        all_items_in_range, count = await self.db.all_new_broadcasts()
        # will raise ZeroDivisionError
        if count == 0:
            return
        all_frontend_sessions, _ = await self.db.all_frontend_sessions()
        # Nowhere to send
        if not all_frontend_sessions:
            return
        async with aiohttp.ClientSession() as session:
            # Send broadcast in the next minute, but not all at the same time
            send_at_list = [(60 / count) * i for i in range(count)]
            await asyncio.gather(
                *[
                    self.send_broadcast(send_at, message, all_frontend_sessions, session)
                    for send_at, message in zip(send_at_list, all_items_in_range)
                ]
            )

    async def send_broadcast(
        self,
        send_at: float,
        broadcast_message: BroadcastMessage,
        frontend: List[Session],
        session: aiohttp.ClientSession
    ) -> None:
        await asyncio.sleep(send_at)
        context = json.loads(broadcast_message["context"])
        tasks = list()
        for each_session in frontend:
            context["chat"]["chat_id"] = each_session["broadcast"]
            tasks.append(session.post(each_session["url"], json=context))
        await asyncio.gather(*tasks)
        # Remove done broadcast task
        await self.db.remove_broadcast(broadcast_message)

    def load_bots_file(self):
        with open(os.path.join(ROOT_PATH, "server", "archive", "latest.json")) as source:
            self.bots_data = json.load(source)
            self.strings.update_strings()

    async def task_load_bots_file(self):
        self.load_bots_file()
