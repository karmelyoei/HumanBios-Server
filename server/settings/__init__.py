from . import settings
from .settings import *  # noqa: F403, F401
from .settings import SERVER_SECURITY_TOKEN, DEBUG
from collections import namedtuple as namedtuple
from typing import Dict
import logging


Config = namedtuple("Config", ["token", "url"])


tokens: Dict[str, Config] = {
    "server": Config(SERVER_SECURITY_TOKEN, ""),
    # Bot for tests, don't touch
    "tests_dummy_bot": Config("TEST_BOT_1111", "http://dummy_url"),
}

__all__ = settings.__all__ + ("Config", "tokens")


# Logging
formatter = "%(asctime)s - %(filename)s - %(levelname)s - %(message)s"
date_format = "%d-%b-%y %H:%M:%S"

logging.basicConfig(
    format=formatter, datefmt=date_format,
    level=logging.DEBUG if DEBUG else logging.INFO
)
