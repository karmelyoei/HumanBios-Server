from typing import Optional
import logging
import pathlib
import dotenv
import os
import ast

dotenv.load_dotenv(".env")
# Three steps back from current file
ROOT_PATH = pathlib.Path(os.path.abspath(__file__)).parent.parent.parent

SERVER_SECURITY_TOKEN = os.environ["SERVER_SECURITY_TOKEN"]

CLOUD_TRANSLATION_API_KEY = os.environ["CLOUD_TRANSLATION_API_KEY"]

AI_URL = os.environ["AI_URL"]
DATABASE_URL = os.environ["DATABASE_URL"]

AWS_ACCESS_KEY_ID = os.environ["AWS_ACCESS_KEY_ID"]
AWS_SECRET_ACCESS_KEY = os.environ["AWS_SECRET_ACCESS_KEY"]

RASA_URL = os.environ["RASA_URL"]
BOTSOCIETY_API_KEY = os.environ["BOTSOCIETY_API_KEY"]

SERVER_HOST = os.environ.get("SERVER_HOST", "") or "0.0.0.0"
SERVER_PORT = int(os.environ.get("SERVER_PORT", False) or 8080)

N_CORES = int(os.environ["N_CORES"])

try:
    DEBUG: Optional[bool] = bool(ast.literal_eval(os.environ["DEBUG"]))
except (ValueError, SyntaxError):
    DEBUG = None
    logging.exception(f"Expected bool, got {os.environ['DEBUG']}")

try:
    TEST_ALLOW_NETWORK_LOAD: Optional[bool] = bool(ast.literal_eval(os.environ["TEST_ALLOW_NETWORK_LOAD"]))
except (ValueError, SyntaxError):
    TEST_ALLOW_NETWORK_LOAD = None
    logging.exception(f"Expected bool, got {os.environ['TEST_ALLOW_NETWORK_LOAD']}")

try:
    TEST_ALLOW_CPU_LOAD: Optional[bool] = bool(ast.literal_eval(os.environ["TEST_ALLOW_CPU_LOAD"]))
except (ValueError, SyntaxError):
    TEST_ALLOW_CPU_LOAD = None
    logging.exception(f"Expected bool, got {os.environ['TEST_ALLOW_CPU_LOAD']}")

try:
    DISABLE_ACCESS_TOKENS: Optional[bool] = bool(ast.literal_eval(os.environ["DISABLE_ACCESS_TOKENS"]))
except (ValueError, SyntaxError):
    DISABLE_ACCESS_TOKENS = None
    logging.exception(f"Expected bool, got {os.environ['DISABLE_ACCESS_TOKENS']}")

OWNER_HASH = os.environ.get("OWNER_HASH", None)

__all__ = (
    "ROOT_PATH",
    "SERVER_SECURITY_TOKEN",
    "CLOUD_TRANSLATION_API_KEY",
    "AI_URL",
    "DATABASE_URL",
    "AWS_ACCESS_KEY_ID",
    "AWS_SECRET_ACCESS_KEY",
    "RASA_URL",
    "BOTSOCIETY_API_KEY",
    "SERVER_HOST",
    "SERVER_PORT",
    "N_CORES",
    "DEBUG",
    "OWNER_HASH",
    "TEST_ALLOW_NETWORK_LOAD",
    "TEST_ALLOW_CPU_LOAD",
    "DISABLE_ACCESS_TOKENS",
)
