from enum import IntEnum, IntFlag, unique
from typing import Literal


@unique
class AccountType(IntEnum):
    COMMON = 1
    MEDIC  = 2
    SOCIAL = 3


class PermissionLevel(IntFlag):
    # keep synched with web/templates/admin/permissions.html
    ANON    = 0
    DEFAULT = 1
    ADMIN   = 2
    MAX     = 5


PeriodType = Literal["one-time", "daily", "weekly"]


class ServiceTypes:
    TELEGRAM = "telegram"
    FACEBOOK = "facebook"
    WEBSITE  = "website"
