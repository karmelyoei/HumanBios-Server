from typing import TypedDict, Dict, Union, List, Optional, Any
from .enums import AccountType, PermissionLevel

# TODO: Make type for schema (for static analysis)
"""
# Sync this with 'server/schema/schema.json'
class RequestType(TypedDict):
    security_token: str
    via_instance:   str
    service_in:     str
    user: UserType
    chat: ChatType

    class UserType:
        pass

    class ChatType:
        pass
"""


class User(TypedDict):
    user_id:      str
    service:      str
    identity:     str
    via_instance: str
    first_name:   Optional[str]
    last_name:    Optional[str]
    username:     str
    language:     str
    account_type: AccountType
    created_at:   str
    location:     Optional[str]
    last_active:  str
    conversation: Optional[str]
    answers:      Dict[str, Any]
    files:        Dict[str, Union[str, list]]
    states:       List[str]
    permissions:  PermissionLevel
    context:      Dict[str, Any]


class Conversation(TypedDict):
    id:    str
    users: Dict[str, str]
    type:  AccountType
    created_at: str


class ConversationRequest(TypedDict):
    identity:     str
    account_type: AccountType
    created_at:   str


class CheckBack(TypedDict):
    id: str
    server_mac: str
    identity: str
    context:  str
    send_at:  str
    chat_id:  int
    state:    str
    period:   str


class AccessToken(TypedDict):
    token:    str
    identity: str


# TODO: Re-work concept of rooms to be more abstract and automatic, allow for creation of "room" with certain permission level (e.g. "TechnicalSupport" or "Doctor").
class Session(TypedDict):
    name:  str
    token: str
    url:   str
    broadcast:          Optional[int]
    psychological_room: Optional[int]
    doctor_room:        Optional[int]


class BroadcastMessage(TypedDict):
    id: str
    context: str


# class TranslationItem(TypedDict):
#    text: str
#    language: str
#    result_text: str
#    result_language: str


class StringItem(TypedDict):
    language: str
    string_key: str
    content_hash: str
    text: str
    manual: bool
