from .typing_hints import Session, BroadcastMessage, StringItem, AccessToken, CheckBack
from ..settings import AWS_ACCESS_KEY_ID, AWS_SECRET_ACCESS_KEY, DATABASE_URL
from typing import List, Iterable, AsyncGenerator, Tuple, Optional
from .enums import AccountType, PermissionLevel, PeriodType
from boto3.dynamodb.conditions import Key, Attr
from botocore.exceptions import ClientError
from ..utils import custom_default
from ..definitions import Context
from .create_db import create_db
from .typing_hints import User
import datetime
import asyncio
import logging
import secrets
import hashlib
import boto3
import pytz
import uuid
import json


def singleton(cls, *args, **kw):
    instances = {}

    def _singleton():
        if cls not in instances:
            instances[cls] = cls(*args, **kw)
        return instances[cls]

    return _singleton


# TODO: Probably worth it to switch to async solution eventually but
# TODO: right now, it's just PoC solution
@singleton
class Database:
    LIMIT_CONCURRENT_CHATS = 100
    TZ = pytz.utc
    SCRYPT_PARAMS = {"n": 16384, "r": 8, "p": 1}

    def __init__(self, database_url=None, region_name="eu-center-1"):
        self.dynamodb = boto3.resource(
            "dynamodb",
            region_name=region_name,
            endpoint_url=database_url or DATABASE_URL,
            aws_access_key_id=AWS_ACCESS_KEY_ID,
            aws_secret_access_key=AWS_SECRET_ACCESS_KEY,
        )
        # Create db tables
        create_db(self.dynamodb)
        self.Users = self.dynamodb.Table("Users")
        self.Conversations = self.dynamodb.Table("Conversations")
        self.ConversationRequests = self.dynamodb.Table("ConversationRequests")
        self.CheckBacks = self.dynamodb.Table("CheckBacks")
        self.AccessTokens = self.dynamodb.Table("AccessTokens")
        self.Sessions = self.dynamodb.Table("Sessions")
        self.BroadcastMessages = self.dynamodb.Table("BroadcastMessages")
        self.StringItems = self.dynamodb.Table("StringItems")
        self.WebSessions = self.dynamodb.Table("WebSessions")
        self.WebCredentials = self.dynamodb.Table("WebCredentials")
        # Cache
        self.active_conversations = 0
        self.requested_users = set()
        self.mac = str(uuid.getnode())

        self.cached_websessions = dict()
        self.cached_webtokens = dict()

    # High level methods

    async def create_websession(self, identity: str) -> str:
        session_id = secrets.token_urlsafe(32)
        self.WebSessions.put_item(
            Item={
                "identity": identity,
                "session_id": session_id,
                "valid": False
            }
        )
        self.cached_websessions[session_id] = (False, identity)
        return session_id

    async def check_websession(self, session_id: str) -> Tuple[bool, Optional[str]]:
        # First check cache
        try:
            return self.cached_websessions[session_id]
        except KeyError:
            pass
        response = self.WebSessions.get_item(
            Key={"session_id": session_id},
            ProjectionExpression="#idtt, valid",
            ExpressionAttributeNames={"#idtt": "identity"},
        )
        try:
            ret = response["Item"]
        except KeyError:
            return False, None
        self.cached_websessions[session_id] = ret["valid"], ret["identity"]
        return ret["valid"], ret["identity"]

    async def set_websession_status(self, session_id: str, status: bool) -> bool:
        new = self.WebSessions.update_item(
            Key={"session_id": session_id},
            UpdateExpression="SET valid = :valid",
            ExpressionAttributeValues={":valid": status},
            ReturnValues="ALL_NEW"
        )
        self.cached_websessions[session_id] = status, new["Attributes"]["identity"]
        return new["Attributes"]["valid"]

    # Web Credentials

    async def check_webcredentials(self, identity: str, password: bytes) -> bool:
        response = self.WebCredentials.get_item(
            Key={"identity": identity},
            ProjectionExpression="salt, #hash",
            ExpressionAttributeNames={"#hash": "hash"}
        )
        try:
            salt = response["Item"]["salt"].value
            stored_hash = response["Item"]["hash"].value
        except KeyError:
            # If no password is set, this is the first login, so store the creds
            # TODO: maybe a bad idea
            return await self.set_webcredentials(identity, None, password, update=False)
        hashed = hashlib.scrypt(password, salt=salt, **self.SCRYPT_PARAMS)
        return secrets.compare_digest(hashed, stored_hash)

    async def set_webcredentials(self, identity: str, current_password: Optional[bytes], new_password: bytes, update: bool = True) -> bool:
        if current_password is not None and not await self.check_webcredentials(identity, current_password):
            return False
        salt = secrets.token_bytes(64)
        hashed = hashlib.scrypt(new_password, salt=salt, **self.SCRYPT_PARAMS)
        if update:
            self.WebCredentials.update_item(
                Key={"identity": identity},
                UpdateExpression="SET salt = :salt, #hash = :hash",
                ExpressionAttributeNames={"#hash": "hash"},
                ExpressionAttributeValues={":salt": salt, ":hash": hashed},
                ReturnValues="ALL_NEW"
            )
        else:
            self.WebCredentials.put_item(Item={
                "identity": identity,
                "salt": salt,
                "hash": hashed
            })
        return True

    async def create_webtoken(self, identity: str) -> str:
        token = secrets.token_urlsafe()
        self.cached_webtokens[token] = identity
        asyncio.get_event_loop().call_later(15, self._del_token, token)
        return token

    async def check_webtoken(self, token: str) -> Optional[str]:
        logging.info(self.cached_webtokens)
        return self.cached_webtokens.get(token, None)

    def _del_token(self, token):
        try:
            del self.cached_webtokens[token]
        except KeyError:
            pass

    # User methods

    async def create_user(self, item: User):
        """Creates User item in the according table"""
        self.Users.put_item(Item=item)

    async def get_user(self, identity: str) -> Optional[User]:
        """Returns User item by the user identity"""
        return self.Users.get_item(Key={"identity": identity}).get("Item", None)

    async def update_user(self, identity: str, expression: str, names: Optional[dict], values: Optional[dict], user: User = None) -> Optional[User]:
        response = self.Users.update_item(
            Key={"identity": identity},
            UpdateExpression=expression,
            ExpressionAttributeNames=names,
            ExpressionAttributeValues=values,
            ReturnValues="UPDATED_NEW",
        )

        if user and response:
            # If user was passed, update values that were returned from db.
            for key, new_value in response["Attributes"].items():
                user[key] = new_value  # type: ignore
        return response

    async def commit_user(self, user: User):
        self.Users.put_item(Item=user)

    async def scan_users(
        self, condition: Optional[Attr], projection_expression: str,
        attribute_names: Optional[dict] = None, attribute_values: Optional[dict] = None,
    ):
        kwargs = {"FilterExpression": condition or {}}
        while True:
            response = self.Users.scan(
                ProjectionExpression=projection_expression,
                ExpressionAttributeNames=attribute_names or {},
                ExpressionAttributeValues=attribute_values or {},
                **kwargs
            )
            for v in response.get("Items", []):
                yield v
            kwargs["ExclusiveStartKey"] = response.get("LastEvaluatedKey", False)
            if kwargs["ExclusiveStartKey"] is False:
                return

    # Conversations

    # TODO: needs rework
    async def create_conversation(self, user: User, users: dict, type_: AccountType):
        conv_id = str(uuid.uuid4())
        self.Conversations.put_item(
            Item={
                "id": conv_id,
                "users": users,
                "type": type_,
                "created_at": self.now(),
            }
        )
        user["conversation"] = conv_id

    async def get_conversation(self, user: User):
        """Returns Conversation item by the user identity"""
        self.Conversations.get_item(Key={"id": user["conversation"]})["Item"]

    # Conversation Requests

    async def create_request(self, identity: str, type_: AccountType):
        """Creates ConversationRequest item in the according table"""
        if identity in self.requested_users:
            raise ValueError("Identity is already in the conversation!")
        self.requested_users.add(identity)
        self.ConversationRequests.put_item(
            Item={
                "identity": identity,
                "type": type_,
                # @Important: 1) DynamoDB do not support `datetime` type
                # @Important: 2) We can use `>` operator on iso-formatted string, because
                # @Important:    structure of it is correct for left-to-right char comparison
                "created_at": datetime.datetime.now(self.TZ).isoformat(),
            }
        )

    async def get_request_by_user(self, identity: str):
        """Returns ConversationRequest item by the user identity"""
        return self.ConversationRequests.get_item(Key={"identity": identity})["Item"]

    async def del_request_by_user(self, identity: str):
        """Remove ConversationRequest item by the user identity"""
        self.requested_users.remove(identity)
        try:
            self.ConversationRequests.delete_item(
                Key={"identity": identity}
            )
        except ClientError as e:
            # @Important: This exception is not really an error, just says that if we entered
            # @Important: some condition to check, before deleting item - the condition failed
            if e.response["Error"]["Code"] == "ConditionalCheckFailedException":
                logging.exception(e.response["Error"]["Message"])
            else:
                raise
    """
    async def get_waiting_requests(self, waiting: datetime.timedelta):
        now = self.now()
        condition = now - waiting
        response = self.ConversationRequests.query(
            FilterExpression="created_at > :c_at",
            ExpressionAttributeNames={
                ":c_at": {
                    "S": condition.isoformat()
                }
            },
        )
        # TODO: FINISH (add return with unpacked results and update `.requested_users` set with latest data)
    """

    def has_user_request(self, identity: str) -> bool:
        return identity in self.requested_users

    def conversations_limit_reached(self) -> bool:
        """Returns True if the number of current active conversations exceed the defined limit"""
        return self.active_conversations >= self.LIMIT_CONCURRENT_CHATS

    def now(self) -> datetime.datetime:
        return datetime.datetime.now(self.TZ)

    # Checkbacks

    async def create_checkback(self, user: User, context: dict, send_in: datetime.timedelta, title: str, period: PeriodType, state: str = "empty"):  # DynamoDB doesn't accept empty strings..
        """Creates Checkback item in the according table"""
        return self.CheckBacks.put_item(
            Item={
                "id": str(uuid.uuid4()),
                "server_mac": str(uuid.getnode()),
                "identity": context["user"]["identity"],
                "chat_id": context["chat"]["chat_id"],
                "context": json.dumps(context, default=custom_default),
                "send_at": (self.now() + send_in).isoformat(),
                "title": title,
                "period": period,
                "state": state,
            }
        )

    async def update_checkback(self, checkback_id: str, expression: str, values: Optional[dict]):
        return self.CheckBacks.update_item(
            Key={"id": checkback_id},
            UpdateExpression=expression,
            ExpressionAttributeValues=values,
            ReturnValues="ALL_NEW",
        )

    async def all_checkbacks_in_range(self, now: datetime.datetime, until: datetime.datetime) -> Tuple[List[CheckBack], int]:
        # TODO: @Important: query is limited to 1MB, so need to move to pagination eventually
        response = self.CheckBacks.query(
            IndexName="time",
            ProjectionExpression="id, server_mac, send_at, chat_id, title, period, context, #idtt, #stt",
            ExpressionAttributeNames={"#idtt": "identity", "#stt": "state"},
            KeyConditionExpression=Key("server_mac").eq(self.mac)
            & Key("send_at").between(now.isoformat(), until.isoformat()),
        )
        # TODO: 1) Remove old checkbacks, BUT only after 1 hour or so (see 2)
        # TODO: 2) Eventually (after 10-45 minutes, *randomly*) look for checkbacks,
        # TODO:    that are not sent, from all possible server_mac addresses;
        # TODO:    basically if one server dies -> others should pick up checkback
        # TODO:    requests and manage them; why *randomly*, to avoid different servers
        # TODO:    querying at exact same minute -> for that also probably worth to add
        # TODO:    "was_sent" bool to the structure.
        return response["Items"], int(response["Count"])

    async def query_checkbacks_by_chat(self, chat_id: str, page: int, page_size: int) -> Tuple[List[CheckBack], int]:
        response = self.CheckBacks.scan(
            ProjectionExpression="id, server_mac, send_at, chat_id, title, period, context, #idtt, #stt",
            ExpressionAttributeNames={"#idtt": "identity", "#stt": "state"},
            FilterExpression=Attr("chat_id").eq(chat_id),
        )
        return response["Items"][page*page_size : page*page_size + page_size], int(response["Count"])  # noqa: E203, E226

    async def delete_checkback(self, checkback_id: str):
        try:
            return self.CheckBacks.delete_item(
                Key={"id": checkback_id}
            )
        except ClientError as e:
            # @Important: This exception is not really an error, just says that if we entered
            # @Important: some condition to check, before deleting item - the condition failed
            if e.response["Error"]["Code"] == "ConditionalCheckFailedException":
                logging.exception(e.response["Error"]["Message"])
            else:
                raise

    # Bot Access Tokens

    async def create_access_token(self, item: AccessToken):
        self.AccessTokens.put_item(Item=item)

    async def delete_access_token(self, token: str):
        # Unauthorize the user.
        item = self.AccessTokens.get_item(Key={"token": token}).get("Item", None)
        if item is not None:
            await self.update_user(
                item["identity"],
                "SET #permissions = :v",
                {"#permissions": "permissions"},
                {":v": PermissionLevel.ANON},
            )

        try:
            item = self.AccessTokens.delete_item(
                Key={"token": token}
            )
        except ClientError as e:
            # @Important: This exception is not really an error, just says that if we entered
            # @Important: some condition to check, before deleting item - the condition failed
            if e.response["Error"]["Code"] == "ConditionalCheckFailedException":
                logging.exception(e.response["Error"]["Message"])
            else:
                raise

    async def check_access_token(self, token: str) -> bool:
        item = self.AccessTokens.get_item(Key={"token": token}).get("Item", {})
        return item.get("identity", "") == "✅"

    async def update_token_owner(self, identity: str, token: str):
        self.AccessTokens.update_item(
            Key={"token": token},
            UpdateExpression="SET #identity = :v",
            ExpressionAttributeNames={"#identity": "identity"},
            ExpressionAttributeValues={":v": identity},
            ReturnValues="UPDATED_NEW",
        )

    async def query_access_tokens(self, identity: str, page: int, page_size: int) -> Tuple[List[AccessToken], int]:
        response = self.AccessTokens.scan(
            # IndexName="user_search",
            ProjectionExpression="#token, #identity",
            FilterExpression="#identity <> :identity",
            ExpressionAttributeNames={"#token": "token", "#identity": "identity"},
            ExpressionAttributeValues={":identity": identity}
        )
        return response["Items"][page*page_size : page*page_size + page_size], int(response["Count"])  # noqa: E203, E226

    # Sessions

    async def create_session(self, item: Session):
        """Creates Session  item in the according table"""
        self.Sessions.put_item(Item=item)

    async def get_session(self, instance_name: str) -> Optional[Session]:
        """Returns User item by the user identity"""
        return self.Sessions.get_item(Key={"name": instance_name}).get("Item", None)

    async def all_frontend_sessions(self) -> Tuple[List[Session], int]:
        response = self.Sessions.scan()
        return response["Items"], int(response["Count"])

    async def create_broadcast(self, context: Context):
        self.BroadcastMessages.put_item(
            Item={
                "id": str(uuid.uuid4()),
                "context": json.dumps(context.__dict__["request"], default=custom_default),
            }
        )

    async def all_new_broadcasts(self) -> Tuple[List[BroadcastMessage], int]:
        response = self.BroadcastMessages.scan()
        return response["Items"], int(response["Count"])

    async def remove_broadcast(self, item: BroadcastMessage):
        self.BroadcastMessages.delete_item(Key={"id": item["id"]})

    # Translation

    async def create_translation(self, item: StringItem):
        self.StringItems.put_item(Item=item)

    async def get_translation(self, lang: str, key: str) -> Optional[StringItem]:
        """Returns User item by the user identity"""
        response = self.StringItems.get_item(
            Key={
                "language": lang,
                "string_key": key
            }
        )
        return response.get("Item", None)

    async def query_translations(self, lang: str, keys: Iterable[str]) -> List[StringItem]:
        # A hack due to the lack of better way
        query = await asyncio.gather(
            *[self.get_translation(lang, key) for key in keys]
        )
        return [qr for qr in query if qr is not None]

    async def bulk_save_translations(self, items: List[StringItem]):
        await asyncio.gather(*[self.create_translation(item) for item in items])

    async def iter_all_translation(self) -> AsyncGenerator[StringItem, None]:
        response = self.StringItems.scan()
        for each_translation in response["Items"]:
            yield each_translation

        while "LastEvaluatedKey" in response:
            response = self.StringItems.scan(
                ExclusiveStartKey=response["LastEvaluatedKey"]
            )
            for each_translation in response["Items"]:
                yield each_translation
