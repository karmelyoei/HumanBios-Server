from ..settings import RASA_URL, ROOT_PATH
from typing import Optional, Union, List
from ..translation import Translator
from aiohttp import ClientSession
from . import Language
import logging
import iso639
import json


RASA_PATH = ROOT_PATH / "rasa" / "languages" / "csv" / "raw_data" / "country_languages.json"
with open(RASA_PATH) as file_:
    available_langs = json.load(file_)


class NLUWorker:
    GET_ENTITIES_URL = f"{RASA_URL}/model/parse"

    def __init__(self, translation_api: Translator):
        self.tr = translation_api

    async def _detect_entities(self, text: str, http: ClientSession):
        async with http.post(self.GET_ENTITIES_URL, json={"text": text}) as resp:
            data = await resp.json()
            for each_entity in data["entities"]:
                yield each_entity["value"], each_entity["entity"]

    async def detect_language(self, text: str) -> Optional[Union[Language, List[Language]]]:
        async with ClientSession() as http:
            # 1) Try to detect entity using rasa nlu
            async for value, entity in self._detect_entities(text, http):
                if entity in ("language", "country", "country_flag"):
                    raw_language_obj = iso639.find(value)
                    # False positive from rasa (maybe add some strings comparison later
                    if raw_language_obj and raw_language_obj["name"] != "Undetermined":
                        logging.info(
                            f"NLU model detected language: ({text})[{raw_language_obj['name']}]"
                        )
                        return raw_language_obj
                if entity in ("country", "country_flag"):
                    # Returned country name
                    # Our dict must have mapping to the country
                    langs = available_langs.get(value)
                    if langs:
                        logging.info(
                            f"NLU model detected country: ({text})[{entity}]"
                        )
                        langs = (iso639.find(lang) for lang in langs)
                        return [
                            lang_obj
                            for lang_obj in langs
                            if lang_obj and lang_obj["name"] != "Undetermined"
                        ]
            else:
                # 2) Detect what is the language of the speaker
                language = await self.tr.detect_language(text, http)
                logging.info(f"Translator detected language: ({text})[{language}]")
                # If user sent message in his own language and we figured out what is it - case closed
                language = iso639.find(language)
                if language and language["name"] != "Undetermined":
                    return language

        # Unhandled condition
        logging.warning("Unhandled condition in 'detect_language'! text = %s", text)
        return None
