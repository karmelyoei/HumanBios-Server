

class BaseStatus:
    commit = True
    process_next = True
    entry = True

    def __init__(self, commit=True, skip_entry=False):
        """
        Args:
            commit (bool): commit changes to the database
        """
        self.commit = commit
        self.entry = not skip_entry


class END(BaseStatus):
    """
    Status-shortcut that moves user to "final" or "exit" state.
    ENDState should implement this final state and finish converstaion.
    """


class OK(BaseStatus):
    """
    Status which stands for "Do nothing" (and usually wait for user response).
    """


class GO_TO_STATE(BaseStatus):
    """
    Status that tells the handler to switch the state.
    """
    next_state = None

    def __init__(self, next_state, skip_entry=False, commit=True):
        """
        Args:
            next_state (str): name of the new state, which should handle user
            skip_entry (bool): decide whether or not to skip `entry` handler and go straight into `process`
            commit (bool): commit changes to the database
        """
        self.next_state = next_state
        self.entry = not skip_entry
        self.commit = commit


class BACK(BaseStatus):
    """
    Status that tells the handler to go back one state
    """

    def __init__(self, process_next=True, skip_entry=False, commit=True):
        """
        Args:
            process_next (bool): set to False to silently wait for user response
            skip_entry (bool): decide whether or not to skip `entry` handler and go straight into `process`
            commit (bool): commit changes to the database
        """
        self.process_next = process_next
        self.entry = not skip_entry
        self.commit = commit


class NOOP(BaseStatus):
    """
    Status that doesn't do anything, but restarts the conversation flow, so next message will be handled by 'StartState'.
    """

    def __init__(self, process_next=True, skip_entry=False, commit=True):
        """
        Args:
            process_next (bool): set to False to silently wait for user response
            skip_entry (bool): decide whether or not to skip `entry` handler and go straight into `process`
            commit (bool): commit changes to the database
        """
        self.process_next = process_next
        self.entry = not skip_entry
        self.commit = commit
