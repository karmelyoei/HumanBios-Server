from typing import Union, Text, Optional, List, Dict


class Button:
    def __init__(self, text: str, key: str = None):
        self.text = text
        self.key = key

    def set_key(self, key: str):
        self.key = key

    def __eq__(self, other: object) -> bool:
        if isinstance(other, Button):
            return self.key == other.key
        if isinstance(other, str):
            return self.key == other
        return NotImplemented

    def __bool__(self):
        return bool(self.key)

    def __repr__(self):
        return f"Button(key={self.key})"

    def __hash__(self):
        return hash(self.key)


class TextPromise:
    def __init__(self, lang: str, key: str):
        self.lang: str = lang
        self.key:  str = key
        self.value: Optional[str] = None
        self.changed = False
        self._format_args:   List[Union[TextPromise, str]]      = []
        self._format_kwargs: Dict[str, Union[TextPromise, str]] = {}
        self._complex:       List[Union[TextPromise, str]]      = []

    @property
    def text(self):
        return str(self)

    def fill(self, value: str):
        self.value = value

    def format(self, *args, **kwargs) -> "TextPromise":
        self._format_args += args
        self._format_kwargs.update(**kwargs)
        for i in range(len(self._complex)):
            self._complex[i] = self._complex[i].format(*args, **kwargs)
        return self

    def set_language(self, lang: str):
        self.lang = lang
        self.changed = True

    def __str__(self):
        value = self.value
        if self._format_args or self._format_kwargs:
            value = value.format(*self._format_args, **self._format_kwargs)
        if self._complex:
            value += "".join(str(item) for item in self._complex)
        return value or f"EmptyTextPromise(key={self.key})"

    def __add__(self, other: Union["TextPromise", Text]):
        self._complex.append(other)
        return self

    def __eq__(self, other):
        return self.key == other.key

    def __hash__(self):
        return hash(self.key)

    # Workaround to make promise to keep itself
    # TODO: remove
    def __deepcopy__(self, memdict={}):  # noqa: B006
        return self

    def __copy__(self):
        return self
