from .items import TextPromise, Button
from ..translation import Translator
from ..settings import ROOT_PATH
from typing import List
import asyncio
import hashlib
import logging
import json
import os


__all__ = ("TextPromise", "Button", "StringAccessor", "Strings")


def load(*path):
    path = os.path.join(ROOT_PATH, *path)
    with open(path) as strings:
        return json.load(strings)


def load_strings():
    # Load our static strings file
    strings = load("server", "strings", "json", "strings.json")
    # Find and load botsociety json file
    if os.path.exists(os.path.join(ROOT_PATH, "server", "archive", "latest.json")):
        # For each message (and it's buttons) add text with correspoding key
        for message in load("server", "archive", "latest.json"):
            strings[message["text_key"]] = message["text"]
            if message["buttons"]:
                for btn in message["buttons"]:
                    strings[btn["text_key"]] = btn["text"]
    # Original set of strings (no translations)
    original_strings = {}
    for each_key in strings:
        original_strings[each_key] = {
            "text": strings[each_key],
            # Generate hash, so when we pull cached translation from database
            # we can check if it's still valid (original text didn't change)
            "hash": hashlib.sha1(strings[each_key].encode()).hexdigest(),
        }
    return original_strings


class StringAccessor:
    def __init__(self, lang: str, strings: "Strings"):
        self.lang = lang
        self.promises: List[TextPromise] = list()
        self.strings = strings

    def __getitem__(self, key: str) -> TextPromise:
        promise = TextPromise(self.lang, key)
        res = self.strings[self.lang].get(key)
        if res is not None:
            promise.fill(res["text"])
        self.promises.append(promise)
        return promise

    async def fill_promises(self):
        # logging.info("Inside \"fill_promises\" function..")
        to_translate = [p for p in self.promises if not p.value or p.changed]
        if to_translate:
            # logging.info(f"Working on promises: {to_translate}.")
            await self.strings.get_translations(to_translate)
        # else:
        #     logging.info("No empty or changed promises found..")
        # logging.info("Leaving \"fill_promises\" function..")


class Strings:
    def __init__(self, translation: Translator, db):
        self.tr = translation
        self.db = db

        self.original_strings = load_strings()
        self.cache = {"en": self.original_strings}
        # Load all translations from db into cache before start up
        # TODO: run this manually from all places this is instantiated -- asyncio.run is NOT appropriate.
        asyncio.run(self.load_everything())

    # Get language cache
    def __getitem__(self, key: str):
        return self.cache.get(key, {})

    def update_strings(self):
        # Reload strings
        self.original_strings = load_strings()
        # Make sure to update pointer, but DO NOT rewrite whole cache dict
        self.cache["en"] = self.original_strings

    def cache_item(self, lang: str, key: str, text: str):
        if lang in self.cache:
            self.cache[lang][key] = text
        else:
            self.cache[lang] = {key: text}

    # Get language cache
    def get(self, key: str):
        return self.cache.get(key)

    # TODO: Since we now can have a situation where strings are changed on running server
    #       we have to account for that, so we should check validity of translations not
    #       only while fetching from database, but from the in-memory cache as well.
    async def get_translations(self, promises: List[TextPromise]) -> List[TextPromise]:
        # logging.info(f"Started translating promises: {promises}..")
        uncached = list()
        for p in promises:
            # If in cache -> serve translations immediately
            cached_res = self.cache.get(p.lang, {}).get(p.key)
            if cached_res is not None and cached_res["hash"] == self.original_strings[p.key]["hash"]:
                p.fill(cached_res["text"])
            else:
                # Otherwise check for cached in db
                db_res = await self.db.get_translation(p.lang, p.key)
                # If hash matches correctly - valid translation
                if db_res is not None and db_res["content_hash"] == self.original_strings[p.key]["hash"]:
                    p.fill(db_res["text"])
                    # update cache value here as well
                    self.cache_item(p.lang, p.key, db_res["text"])
                # Finally, make new translation
                uncached.append(p)

        # Update invalid translations (or add missing ones)
        if uncached:
            await self.tr.translate_promises(uncached, self.original_strings)
            # Save changes to cache and db
            to_db = []
            for p in uncached:
                to_db.append({
                    "language": p.lang,
                    "string_key": p.key,
                    # We save original string's hash, not own hash because
                    # we will be comparing to original later
                    "content_hash": self.original_strings[p.key]["hash"],
                    "text": p.text,
                })
                self.cache_item(p.lang, p.key, p.text)
            # create db task (db cache in eventually-consistent manner is okay)
            asyncio.ensure_future(self.db.bulk_save_translations(to_db))

        # logging.info(f"Done translating promises: {promises}..")
        # Returning updated result
        return promises

    async def load_everything(self):
        count = 0
        async for each_translation in self.db.iter_all_translation():
            if self.cache.get(each_translation["language"]):
                self.cache[each_translation["language"]][each_translation["string_key"]] = {
                    "text": each_translation["text"],
                    "hash": each_translation["content_hash"]
                }
            else:
                self.cache[each_translation["language"]] = {
                    each_translation["string_key"]: {
                        "text": each_translation["text"],
                        "hash": each_translation["content_hash"]
                    }
                }
            count += 1
        logging.info(f"Loaded {count} translated items from database.")
