from ..definitions import Context
from . import BaseState, OK
from ..db import User


class NewsCheckbackState(BaseState):
    has_entry = False

    # @Important: This state purposely resets whole dialog
    async def process(self, context: Context, user: User, db):
        # raw_answer = context['request']['message']['text']
        # button = self.parse_button(raw_answer)
        # if button == 'yes':
        #     return base_state.GO_TO_STATE("QAState")
        # elif button == 'no':
        #     # Add the previous state to the stack (aka return user to the bothered state)
        #     return base_state.GO_TO_STATE(user['states'][-2])

        # Bad answer
        # context['request']['message']['text'] = self.strings["qa_error"]
        # context['request']['has_buttons'] = False
        # self.send(user, context)
        return OK
