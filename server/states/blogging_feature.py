from ..db import User, PermissionLevel
from ..definitions import Context
from . import BaseState, OK, BACK


class BloggingState(BaseState):
    # Only respond to an admin.
    permission_required = PermissionLevel.ADMIN

    async def entry(self, context: Context, user: User, db):
        context["request"]["message"]["text"] = self.strings["edit_story"] + user["answers"]["story"]
        context["request"]["buttons_type"] = "text"
        context["request"]["buttons"] = [
            {"text": self.strings["edit"]},
            {"text": self.strings["pass"]},
            {"text": self.strings["stop"]},
        ]
        self.send(user, context)
        user["context"]["blogging"] = "edit"
        return OK

    async def process(self, context: Context, user: User, db):
        button = self.parse_button(context["request"]["message"]["text"])

        if user["context"]["blogging"] == "edit":
            if button == "edit":
                context["request"]["message"]["text"] = self.strings["edit_confirmed"]
                context["request"]["buttons"] = []

                self.send(user, context)
                user["context"]["blogging"] = "fine"
                return OK
            else:
                raise NotImplementedError("Poolitzer didn't implement it.")

        if user["context"]["blogging"] == "fine":
            if button != "pass":
                user["answers"]["story"] = context["request"]["message"]["text"]
            context["request"]["message"]["text"] = self.strings["confirm_broadcast"]
            context["request"]["buttons"] = []

            self.send(user, context)
            del user["context"]["blogging"]
            text = self.strings["broadcast_message"].format(user["first_name"]) + user["answers"]["story"]
            context["request"]["message"]["text"] = text
            self.create_task(db.create_broadcast, context)
            return BACK
