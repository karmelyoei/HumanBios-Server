from ..definitions import Context
from . import BaseState, OK
from ..db import User


class ENDState(BaseState):
    has_entry = False

    # @Important: This state purposely resets whole dialog
    async def process(self, context: Context, user: User, db):
        # End conversation message, no buttons
        context["request"]["message"]["text"] = self.strings["end_convo"]
        context["request"]["buttons"] = []
        self.send(user, context)
        # Reset the flow
        user["context"]["bq_state"] = 1
        # Clear list of states related to the user
        user["states"] = ["StartState"]
        # The next message from user will trigger StartState
        return OK
