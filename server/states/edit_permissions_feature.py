from ..db import User, PermissionLevel
from . import BaseState, OK, END, BACK
from ..definitions import Context


class EditPermissionsState(BaseState):
    # Only respond to an admin.
    permission_required = PermissionLevel.ADMIN

    async def entry(self, context: Context, user: User, db):

        # Explain usage of broadcast targets
        context["request"]["message"]["text"] = self.strings["edit_permissions_identity"]
        context["request"]["buttons_type"] = "text"
        context["request"]["buttons"] = [
            {"text": self.strings["back"]},
            {"text": self.strings["stop"]},
        ]
        # Don't forget to add task
        self.send(user, context)
        user["context"]["editing_permissions"] = "identity"
        return OK

    async def process(self, context: Context, user: User, db):
        text = context["request"]["message"]["text"]
        button = self.parse_button(text)

        if button == "stop":
            return END
        elif button == "back":
            return BACK
        elif user["context"]["editing_permissions"] == "identity":
            user["context"]["editing_permissions_identity"] = text.strip()
            user["context"]["editing_permissions"] = "action"
            await self._list_actions(context, user)
            return OK
        elif user["context"]["editing_permissions"] == "action":
            level = {
                "anon": PermissionLevel.ANON,
                "default": PermissionLevel.DEFAULT,
                "admin": PermissionLevel.ADMIN,
            }.get(str(button.key), None)
            if level is None:
                await self._list_actions(context, user)
                return OK

            if user["context"]["editing_permissions_identity"] in ("me", user["identity"]):
                # fsm always commits after the process, so we have to do this seperately
                user["permissions"] = level
            else:
                await db.update_user(
                    user["context"]["editing_permissions_identity"],
                    "SET #permissions = :v",
                    {"#permissions": "permissions"},
                    {":v": level},
                )
            del user["context"]["editing_permissions"]
            return BACK

    async def _list_actions(self, context: Context, user: User):
        context["request"]["message"]["text"] = self.strings["edit_permissions_action"]
        context["request"]["buttons_type"] = "text"
        context["request"]["buttons"] = [
            {"text": self.strings["anon"]},
            {"text": self.strings["default"]},
            {"text": self.strings["admin"]},
            {"text": self.strings["back"]},
            {"text": self.strings["stop"]},
        ]
        # Don't forget to add task
        self.send(user, context)
