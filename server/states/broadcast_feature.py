from typing import Optional, Dict, List, Any
from boto3.dynamodb.conditions import Attr
import ast

from . import BaseState, OK, BACK, END
from ..db import User, PermissionLevel
# from db.enums import PermissionLevel
from ..definitions import Context


class BroadcastingState(BaseState):
    # Only respond to an admin.
    permission_required = PermissionLevel.ADMIN

    async def entry(self, context: Context, user: User, db):
        # Make sure to save previous state with fallback for the index error
        if user["permissions"] < PermissionLevel.ADMIN:
            return BACK

        # Explain usage of broadcast targets
        context["request"]["message"]["text"] = self.strings["broadcast_target_help"]
        context["request"]["buttons_type"] = "text"
        context["request"]["buttons"] = [
            {"text": self.strings["back"]},
            {"text": self.strings["stop"]},
        ]
        # Don't forget to add task
        self.send(user, context)
        user["context"]["broadcasting"] = "target"
        return OK

    async def process(self, context: Context, user: User, db):
        if user["permissions"] < PermissionLevel.ADMIN:
            # again, just in case the fsm has a bug letting people bypass entry
            #     p.s. no, it doesn't
            return BACK

        text = context["request"]["message"]["text"]
        # Note: we don't need "truncated" here because facebook only
        # truncates *buttons* (quickreplies) 20+ characters long
        button = self.parse_button(text)

        if button == "stop":
            return END
        elif button == "back":
            return BACK

        elif user["context"]["broadcasting"] == "target":
            broadcasting_to: Optional[Dict[str, List[Any]]] = None
            if text == "*":
                broadcasting_to = {}
            else:
                broadcasting_to = self._parse_targets(text)  # type: ignore

            if broadcasting_to is None:
                # Targets are not parseable
                context["request"]["message"]["text"] = self.strings["broadcast_targets_error"]
            else:
                # Request broadcast message
                context["request"]["message"]["text"] = self.strings["broadcast_get_message"]
                user["context"]["broadcasting_to"] = broadcasting_to
                user["context"]["broadcasting"] = "send"

            context["request"]["buttons"] = []
            # Don't forget to add task
            self.send(user, context)
            return OK

        elif user["context"]["broadcasting"] == "send":
            context["request"]["buttons"] = []
            count = 0
            async for target in self._get_targets(user["context"]["broadcasting_to"], db):
                context["request"]["chat"]["chat_id"] = int(target["user_id"])
                self.send(user, context, allow_gather=True)
                count += 1
            context["request"]["message"]["text"] = self.strings["broadcast_complete"].format(count)
            self.send(user, context)
            # Not strictly needed, but it's still a good idea to remove state specific data
            del user["context"]["broadcasting"]
            del user["context"]["broadcasting_to"]
            return BACK

    def _parse_targets(self, message: Optional[str]):
        if message is None:
            return

        key = None
        data = {}
        for cond in message.split("\n"):
            if not cond:
                continue

            invert = cond[0] == "!"
            mode = None
            for i, char in enumerate(cond):
                if char in "=<>~{}":
                    mode = char
                    key = cond[:i + 1]
                    try:
                        value = ast.literal_eval(cond[i + 1:])
                        if not isinstance(value, (str, int)):
                            if isinstance(value, list):
                                if any([lambda item: not isinstance(item, (str, int)), value]):
                                    value = cond[i + 1:]
                            else:
                                value = cond[i + 1:]
                    except (ValueError, SyntaxError):
                        value = cond[i + 1:]
                    break
            if mode is None:
                return
            data[key] = [invert, mode, value]  # dynamodb can't serialise sets
        return data

    def _get_targets(self, parsed_targets, db):
        overall_cond = None
        for key, (invert, type_, value) in parsed_targets.items():
            cond = None
            attr = Attr(key)
            if type_ == "=":
                cond = attr.eq(value)
            elif type_ == "<":
                cond = attr.lt(value)
            elif type_ == ">":
                cond = attr.lte(value)
            elif type_ == "{":
                cond = attr.is_in(value)
            elif type_ == "}":
                cond = attr.contains(value)
            if invert:
                cond = ~cond
            if overall_cond is None:
                overall_cond = cond
            else:
                overall_cond &= cond
        # Return async generator
        return db.scan_users(overall_cond, "via_instance, user_id")
