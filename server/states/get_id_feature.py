from ..db import User, PermissionLevel
from ..definitions import Context
from . import BaseState, NOOP


class GetIdState(BaseState):
    has_entry = False
    # Allow anyone to access this.
    permission_required = PermissionLevel.ANON

    async def process(self, context: Context, user: User, db):
        context["request"]["message"]["text"] = self.strings["your_identity"].format(user["identity"])
        self.send(user, context)
        return NOOP
