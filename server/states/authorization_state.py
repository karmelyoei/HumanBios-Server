from . import BaseState, OK, BACK, NOOP
from ..db import User, PermissionLevel
from ..definitions import Context
import secrets
import math


class AuthorizationState(BaseState):
    has_entry = False
    # Allow anyone to access this.
    permission_required = PermissionLevel.ANON

    async def process(self, context: Context, user: User, db):
        text  = context["request"]["message"]["text"]
        token = text[len("/login"):].strip("\n ")

        if user["permissions"] > PermissionLevel.ANON:
            context["request"]["message"]["text"] = self.strings["authorized_repeat"]
            context["request"]["buttons"] = []
            self.send(user, context)
            return NOOP

        elif token and await self.db.check_access_token(token):
            user["permissions"] = PermissionLevel.DEFAULT
            await self.db.update_token_owner(user["identity"], token)

            context["request"]["message"]["text"] = self.strings["authorized_done"].format(token[:10])
            context["request"]["buttons"] = []
            self.send(user, context)
            return NOOP

        else:
            context["request"]["message"]["text"] = self.strings["authorized_base"]
            context["request"]["buttons"] = []
            self.send(user, context)
            return NOOP


# Responder to /new_access_token command.
class NewAccessTokenState(BaseState):
    has_entry = False
    # Only respond to an admin.
    permission_required = PermissionLevel.ADMIN

    async def process(self, context: Context, user: User, db):
        # Generate token:
        token = secrets.token_hex(32)
        # Storing token to db.
        await self.db.create_access_token({"token": token, "identity": "✅"})  # ✅ Stands for "empty"
        # Reply with the token.
        context["request"]["message"]["text"] = self.strings["authorized_new_token"].format(token)
        context["request"]["buttons"] = []
        self.send(user, context)
        return NOOP


class AccessTokensMenuState(BaseState):
    # Only respond to an admin.
    permission_required = PermissionLevel.ADMIN

    async def entry(self, context: Context, user: User, db):
        user["context"]["tokens_menu"] = {
            "page": 0,
            "page_size": 9,
            "total_approx": None,
            "current_page": {},
        }
        await self.send_page(context, user)
        return OK

    async def process(self, context: Context, user: User, db):
        text   = context["request"]["message"]["text"]
        button = self.parse_button(text)

        # Turning serialized "Decimal" back into "int". Remember: "Decimal(-1) % 3" evaluates to "-1"..
        page      = int(user["context"]["tokens_menu"]["page"])
        page_size = int(user["context"]["tokens_menu"]["page_size"])
        total     = int(user["context"]["tokens_menu"]["total_approx"])

        if button.key == "next":
            user["context"]["tokens_menu"]["page"] = (page + 1) % math.ceil((total or 1) / page_size)
            await self.send_page(context, user)
            return OK

        elif button.key == "previous":
            user["context"]["tokens_menu"]["page"] = (page - 1) % math.ceil((total or 1) / page_size)
            await self.send_page(context, user)
            return OK

        elif button.key == "back":
            return BACK

        token = user["context"]["tokens_menu"]["current_page"].get(text, False)
        if text and len(text) == 10 and token:
            await self.db.delete_access_token(token)
            await self.send_page(context, user)
            return OK

        return NOOP

    async def send_page(self, context: Context, user: User):
        identity  = user["identity"]
        # Turning serialized "Decimal" back into "int". Remember: "Decimal(-1) % 3" evaluates to "-1"..
        page      = int(user["context"]["tokens_menu"]["page"])
        page_size = int(user["context"]["tokens_menu"]["page_size"])

        text, total, buttons = await self.get_page(user, identity, page, page_size)
        # Special case for when all tokens on this page were deleted, but in general, there are more tokens left to display.
        if not text and total:
            page -= 1
            text, total, buttons = await self.get_page(user, identity, page, page_size)

        context["request"]["message"]["text"] = self.strings["authorized_list_tokens"].format(
            text = text,
            page = page + 1,  # Display page, range starts from 1.
            total_size = math.ceil((total or 1) / page_size),
        )
        context["request"]["buttons"] = buttons
        # Store for reuse:
        user["context"]["tokens_menu"]["total_approx"] = total
        self.send(user, context)

    async def get_page(self, user: User, identity: str, page: int, page_size: int):
        # Clear space for tokens in the context.
        user["context"]["tokens_menu"]["current_page"].clear()

        # Query tokens from db
        tokens, total = await self.db.query_access_tokens(identity, page, page_size)
        text, buttons, tmp = None, [], []  # type: ignore [var-annotated]
        for i, item in enumerate(tokens):
            line = self.strings["authorized_list_token"].format(
                index = page * page_size + i + 1,
                token = item["token"][:10],
                user  = item["identity"],
            )
            if text is None:  text = line
            else:             text += line  # type: ignore [unreachable]

            if i % 3 == 0:
                if tmp:  buttons.append(tmp)
                tmp = list()
            tmp.append({"text": item["token"][:10]})

            # Store tokens in the context.
            user["context"]["tokens_menu"]["current_page"][item["token"][:10]] = item["token"]

        if tmp:  buttons.append(tmp)

        buttons.append([
            {"text": self.strings["previous"]},
            {"text": self.strings["back"]},
            {"text": self.strings["next"]},
        ])
        return text, total, buttons
