from ..utils import custom_default
from ..definitions import Context
from . import BaseState, OK, END
from ..settings import AI_URL
from ..db import User
import aiohttp
import logging
import json


class AIState(BaseState):
    async def entry(self, context: Context, user: User, db):
        context["request"]["message"]["text"] = self.strings["ai_notice"]
        context["request"]["buttons"] = [{"text": self.strings["stop"]}]
        self.send(user, context)
        return OK

    async def process(self, context: Context, user: User, db):
        # Alias for text answer
        raw_answer = context["request"]["message"]["text"]
        # Parse button to have easy access to intent
        button = self.parse_button(raw_answer)
        if button == "stop":
            return END

        resp = await self.get_response(user["identity"], raw_answer)

        context["request"]["message"]["text"] = resp
        context["request"]["buttons"] = [{"text": self.strings["stop"]}]
        self.send(user, context)
        return OK

    async def get_response(self, user_id, prompt):
        async with aiohttp.ClientSession() as session:
            async with session.post(
                f"{AI_URL}/api/get_response",
                json=json.dumps({"user_id": user_id, "text": prompt}, default=custom_default)
            ) as resp:
                res = await resp.json()
                # [INFO]
                user_id = f"{user_id[:8]}.."
                logging.info(f"AIState(input={{'user_id': '{user_id}', 'prompt': '{prompt}'}}, response={res}")
                return res["text"]
