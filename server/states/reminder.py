from datetime import datetime, timedelta
from . import BaseState, OK, BACK, NOOP
from ..definitions import Context
from typing import Optional
from copy import deepcopy
from ..db import User
import logging
import math


class ReminderState(BaseState):
    COMMAND = "/remind"

    async def entry(self, context: Context, user: User, db):
        context["request"]["message"]["text"] = self.strings["reminder_init"]
        context["request"]["buttons"] = [
            {"text": self.strings["new"]},
            {"text": self.strings["list"]},
        ]
        self.send(user, context)

        user["context"]["reminder"] = {
            "next_state": "reminder_init",
            "title":  None,
            "body":   None,
            "period": None,
            "time":   None,
        }
        return OK

    async def process(self, context: Context, user: User, db):
        text = context["request"]["message"]["text"]
        button = self.parse_button(text)
        next_state = user["context"]["reminder"]["next_state"]

        if next_state == "reminder_init" and button.key == "new":
            user["context"]["reminder"]["next_state"] = "reminder_body"

            context["request"]["message"]["text"] = self.strings["reminder_title"]
            context["request"]["buttons"] = []
            self.send(user, context)
            return OK

        elif next_state == "reminder_body":
            # TODO: add character limit (something like 256)
            user["context"]["reminder"]["title"] = text
            user["context"]["reminder"]["next_state"] = "reminder_repeat"

            context["request"]["message"]["text"] = self.strings["reminder_body"]
            context["request"]["buttons"] = []
            self.send(user, context)
            return OK

        elif next_state == "reminder_repeat":
            # TODO: add character limit (something like 2048)
            user["context"]["reminder"]["body"] = text
            user["context"]["reminder"]["next_state"] = "reminder_time"

            context["request"]["message"]["text"] = self.strings["reminder_repeat"]
            context["request"]["buttons"] = [
                {"text": self.strings["one-time"]},
                {"text": self.strings["daily"]},
                {"text": self.strings["weekly"]},
            ]
            self.send(user, context)
            return OK

        elif next_state == "reminder_time" and button.key in ["one-time", "daily", "weekly"]:
            user["context"]["reminder"]["period"] = button.key
            user["context"]["reminder"]["next_state"] = "reminder_check"

            utc_time = datetime.now().strftime("%H:%M (%d/%m/%Y)")
            if button.key == "one-time":
                context["request"]["message"]["text"] = self.strings["reminder_time_once"].format(utc_time)
            else:
                context["request"]["message"]["text"] = self.strings["reminder_time_regular"].format(utc_time)
            context["request"]["buttons"] = []
            self.send(user, context)
            return OK

        elif next_state == "reminder_check":
            # Parsing time.
            try:
                dt_obj: Optional[datetime] = datetime.strptime(text, "%H:%M %d/%m/%Y")
            except ValueError:
                # Here we try another format (short format for the year).
                try:
                    dt_obj = datetime.strptime(text, "%H:%M %d/%m/%y")
                except ValueError:
                    # logging.exception(e)  # For now log this one, because might be a pain to debug.
                    dt_obj = None
            # Finally we process date object if it's parsed correctly.
            if dt_obj is not None:
                # Arbitrary data value validation:
                # 1. Must be in the future, so bigger than current datetime.
                if dt_obj < datetime.now():
                    context["request"]["message"]["text"] = self.strings["reminder_err_require_future"]
                    context["request"]["buttons"] = []
                    self.send(user, context)
                    return OK
                # 2. Minimum time until reminder is 10 minutes.
                elif dt_obj - datetime.now() < timedelta(minutes=10):
                    context["request"]["message"]["text"] = self.strings["reminder_err_min_time"].format("10 minutes")
                    context["request"]["buttons"] = []
                    self.send(user, context)
                    return OK

                user["context"]["reminder"]["time"] = dt_obj.isoformat()
                user["context"]["reminder"]["next_state"] = "reminder_done"

                context["request"]["message"]["text"] = self.strings["reminder_check"].format(
                    title=user["context"]["reminder"]["title"],
                    body=user["context"]["reminder"]["body"],
                    period=user["context"]["reminder"]["period"],
                    time=dt_obj.strftime("%H:%M %d/%m/%y"),
                    now=datetime.now().strftime("%H:%M %d/%m/%y"),
                )
                context["request"]["buttons"] = [
                    {"text": self.strings["confirm"]},
                    {"text": self.strings["cancel"]},
                ]
                self.send(user, context)
                return OK
            else:
                context["request"]["message"]["text"] = self.strings["reminder_err_date_parsing"]
                context["request"]["buttons"] = []
                self.send(user, context)
                return OK

        elif next_state == "reminder_done" and button.key == "confirm":
            # Prepare message.
            context["request"]["message"]["text"] = self.strings["reminder_base"].format(
                title=user["context"]["reminder"]["title"],
                body=user["context"]["reminder"]["body"],
            )
            context["request"]["buttons"] = []
            self.create_task(
                db.create_checkback, user, deepcopy(context.__dict__["request"]),
                datetime.fromisoformat(user["context"]["reminder"]["time"]) - datetime.now(),
                title=user["context"]["reminder"]["title"],
                period=user["context"]["reminder"]["period"],
            )
            # Remove data after processing state.
            # del user["context"]["reminder"]

            context["request"]["message"]["text"] = self.strings["reminder_done"]
            context["request"]["buttons"] = []
            self.send(user, context)
            # Processing finished, retreat from the state.
            return NOOP

        elif next_state == "reminder_done" and button.key == "cancel":
            # Remove data after processing state.
            # del user["context"]["reminder"]

            context["request"]["message"]["text"] = self.strings["reminder_cancel"]
            context["request"]["buttons"] = []
            self.send(user, context)
            # Processing finished, retreat from the state.
            return NOOP

        # TODO: Everything below is basically the same pagination / menu code as in 'authorization_state' - should be generalized and rewritten.
        elif next_state == "reminder_init" and button.key == "list":
            user["context"]["reminder"]["next_state"] = "reminder_list"

            user["context"]["reminder_menu"] = {
                "page": 0,
                "page_size": 9,
                "total_approx": None,
                "current_page": {},
            }
            await self.send_page(context, user)
            return OK

        elif next_state == "reminder_list" and button.key == "next":
            # Turning serialized "Decimal" back into "int". Remember: "Decimal(-1) % 3" evaluates to "-1"..
            page      = int(user["context"]["reminder_menu"]["page"])
            page_size = int(user["context"]["reminder_menu"]["page_size"])
            total     = int(user["context"]["reminder_menu"]["total_approx"])

            user["context"]["reminder_menu"]["page"] = (page + 1) % math.ceil((total or 1) / page_size)
            await self.send_page(context, user)
            return OK

        elif next_state == "reminder_list" and button.key == "previous":
            # Turning serialized "Decimal" back into "int". Remember: "Decimal(-1) % 3" evaluates to "-1"..
            page      = int(user["context"]["reminder_menu"]["page"])
            page_size = int(user["context"]["reminder_menu"]["page_size"])
            total     = int(user["context"]["reminder_menu"]["total_approx"])

            user["context"]["reminder_menu"]["page"] = (page - 1) % math.ceil((total or 1) / page_size)
            await self.send_page(context, user)
            return OK

        elif next_state == "reminder_list" and text and user["context"]["reminder_menu"]["current_page"].get(text, False):
            await self.db.delete_checkback(user["context"]["reminder_menu"]["current_page"][text])  # Passing here stored value as checkback_id (uuid4 value)
            await self.send_page(context, user)
            return OK

        elif next_state == "reminder_list" and button.key == "back":
            return BACK

        logging.error("Unhandled value of 'next_state': '%s'", next_state)
        return NOOP

    async def send_page(self, context: Context, user: User):
        # Turning serialized "Decimal" back into "int". Remember: "Decimal(-1) % 3" evaluates to "-1"..
        page      = int(user["context"]["reminder_menu"]["page"])
        page_size = int(user["context"]["reminder_menu"]["page_size"])

        text, total, buttons = await self.get_page(user, context, page, page_size)
        # Special case for when all reminders on this page were deleted, but in general, there are more reminders left to display.
        if not text and total:
            page -= 1
            text, total, buttons = await self.get_page(user, context, page, page_size)

        context["request"]["message"]["text"] = self.strings["reminder_list"].format(
            text = text,
            page = page + 1,  # Display page, range starts from 1.
            total_size = math.ceil((total or 1) / page_size),
        )
        context["request"]["buttons"] = buttons
        # Store for reuse:
        user["context"]["reminder_menu"]["total_approx"] = total
        self.send(user, context)

    async def get_page(self, user: User, context: Context, page: int, page_size: int):
        # Clear space for reminders in the context.
        user["context"]["reminder_menu"]["current_page"].clear()

        # Query reminders from db
        reminders, total = await self.db.query_checkbacks_by_chat(context["request"]["chat"]["chat_id"], page, page_size)
        text, buttons, tmp = None, [], []  # type: ignore [var-annotated]
        for i, item in enumerate(reminders):
            line = self.strings["reminder_list_item"].format(
                index = page * page_size + i + 1,
                title = item["title"][:24],  # Will this be sufficient?
            )
            if text is None:  text = line
            else:             text += line  # type: ignore [unreachable]

            if i % 3 == 0:
                if tmp:  buttons.append(tmp)
                tmp = list()
            tmp.append({"text": item["title"][:24]})

            # Store tokens in the context.
            user["context"]["reminder_menu"]["current_page"][item["title"][:10]] = item["id"]

        if tmp:  buttons.append(tmp)

        buttons.append([
            {"text": self.strings["previous"]},
            {"text": self.strings["back"]},
            {"text": self.strings["next"]},
        ])
        return text, total, buttons
