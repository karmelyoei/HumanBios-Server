from ..settings import DEBUG, SERVER_HOST, SERVER_PORT
from ..db import User, PermissionLevel
from urllib.parse import urlunparse
from ..definitions import Context
from . import BaseState, NOOP


class WebAdminLoginState(BaseState):
    has_entry = False
    # Only respond to an admin.
    permission_required = PermissionLevel.ADMIN

    async def process(self, context: Context, user: User, db):
        if user["permissions"] <= PermissionLevel.ADMIN:
            return NOOP

        scheme = "http" if DEBUG else "https"
        token  = await db.create_webtoken(user["identity"])
        port   = ":" + str(SERVER_PORT) if DEBUG else ""

        url = urlunparse((scheme, SERVER_HOST + port, "/admin/auth/" + token, None, None, None))
        context["request"]["message"]["text"] = self.strings["webtoken"].format(url)
        self.send(user, context)
        return NOOP
