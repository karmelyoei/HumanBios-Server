flake8 || exit $?
# Analyze code itself
mypy --strict-equality --cache-dir=/dev/null -p server || exit $?
# Analyze testing code
mypy --strict-equality --cache-dir=/dev/null -p tests || exit $?

# Our own tests
# Make sure to prepare .env file for the tests
if [ ! -f .env ]; then
    cp .env.test .env
fi
# Start database service before testing
if [ ! -z "$DOCKER_CONTAINER" ]; then
  docker network create caddynet
  docker network connect caddynet "$(hostname)"
fi

# Run Database container for testing
docker-compose up --force-recreate --renew-anon-volumes -d db

# TODO: 1) fix all flake8 issues
#       2) enable ai tests
# Run GPT2BOT
# cd gpt2bot
# docker-compose up -d
# cd ..

# Make sure to download latest file of the
# botsociety data from any server available
mkdir -p server/archive
cd server/archive
if [ ! -f latest.json ]; then
    curl -s https://server.catdrew.dev/archive/latest.json -o latest.json || exit $?
fi
cd ../..

# Run Pytest
# @Important: "-W ignore::DeprecationWarning" to disable warnings (to enable: -Wall)
PYTHONDEVMODE=1 pytest -W ignore::DeprecationWarning -p no:cacheprovider || exit $?

# TODO: Create scripts for end-to-end testing.
# python3 ./end2end/test.py
