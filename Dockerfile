ARG VERSION=3.9-slim-buster

# Builder for the wheels
FROM python:${VERSION} as wheel-builder
RUN apt-get -y update \
 && apt-get -y --no-install-recommends install \
    gcc \
    libc-dev

WORKDIR /wheels
COPY requirements.txt .
RUN pip install -U pip wheel setuptools \
 && pip wheel -r requirements.txt


# Main stuff
FROM python:${VERSION} as main

WORKDIR /usr/src/app
COPY --from=wheel-builder /wheels /wheels
COPY requirements.txt .
RUN pip3 install --no-cache-dir --upgrade pip wheel setuptools \
 && pip3 install --no-cache-dir -r requirements.txt -f /wheels

COPY server server
# Rasa countries list file, required for mapping valid values (country_languages.json)
COPY rasa/languages/csv/raw_data/country_languages.json rasa/languages/csv/raw_data/country_languages.json
# Launch
CMD ["python3", "-m", "server"]


# Tester build
FROM main as test

# We dont have to clean up after build, because it's a tester build
RUN apt-get -y update \
 && apt-get -y --no-install-recommends install docker-compose docker.io tidy curl
# COPY requirements.txt .
COPY test-requirements.txt .
RUN pip3 install --no-cache-dir -r test-requirements.txt

COPY . .
# Run tests
ENTRYPOINT [ "bash", "test.sh" ]
