from server.translation import Translator
import pytest


@pytest.fixture
def translator():
    return Translator()


async def mockup_translate_text(text: str, target: str, session=None, from_lang: str = "en"):
    assert text
    assert type(text) == str

    assert target
    assert type(target) == str

    assert from_lang
    assert type(from_lang) == str

    return f"{target}({text})"


test_src = [
    ["ru", {
        "msg1": "Hello",
        "msg2": "Bye"
    }, {}],
    ["es", {
        "ok": "OK",
        "yes": "Yes",
        "no": "No",
        "stop": "Stop"
    }, {}],
    ["uk", {
        "string": "S"
    }, {}],
]


for each in test_src:
    for key in each[1]:
        each[2][key] = f"{each[0]}({each[1][key]})"  # type: ignore [index]


test_mltpl_src = [
    [("pl"), {
        "greet": "Hello"
    }],
    [("de", "he"), {
        "key_1": "Text One",
        "key_2": "Text Two",
        "key_3": "text Three",
    }],
]


for each in test_mltpl_src:
    each.append({lang: {} for lang in each[0]})
    for lang in each[0]:
        for key in each[1]:
            each[2][lang][key] = f"{lang}({each[1][key]})"  # type: ignore [index]


@pytest.mark.asyncio
@pytest.mark.parametrize("lang, texts, correct_dict", test_src)
async def test_translator_translate_dict(lang, texts, correct_dict, monkeypatch, translator):
    # Replace translation method with mock
    monkeypatch.setattr(translator, "translate_text", mockup_translate_text)
    # Do translations
    result = await translator.translate_dict(lang, texts)

    assert result == correct_dict


@pytest.mark.asyncio
@pytest.mark.parametrize("langs, texts, correct_dicts", test_mltpl_src)
async def test_translator_multiple_dicts(langs, texts, correct_dicts, monkeypatch, translator):
    # Replace translation method with mock
    monkeypatch.setattr(translator, "translate_text", mockup_translate_text)
    # Do translations
    result = await translator.translate_to_multiple_languages_dict(langs, texts)

    assert result == correct_dicts
