from server.definitions.status import BaseStatus
import server.states as states_module
from server.__main__ import worker
import inspect


def isstatus(obj) -> bool:
    if not inspect.isclass(obj):
        obj = type(obj)
    return issubclass(obj, BaseStatus)


all_states = {state.__name__: state for state in states_module.collect()}
assert all_states
states = list()

ordered_states = [
    "StartState",
    "QAState",
    "BroadcastingState",
    "GetIdState",
    "WebAdminLoginState",
    "ENDState"
]
for st in ordered_states:
    assert st in all_states
    states.append((st, all_states[st]))

handler = worker.handler
ai_state = all_states["AIState"](
    db=handler.db,
    tr=handler.translator,
    nlu=handler.nlu,
    strings=handler.strings,
    bots_data=handler.bots_data,
)
ai_state.set_language("en")

texts = [
    "start",
    "Accept",
    "Yes",
    "Yes",
    "Yes",
    "Yes",
    "No",
    "Yes",
    "/start",
    "Hi",
    "Back",
    "Stop",
    "Some random medium size text that should not cause any problems",
    "AAAAA" * 25,
    "бабушка",
    "\xac\x48\xfd\xd1\x70\x4c\x40\xf5\xe9\x40\xc4\x0a\xe5\x5c\x2c\x2c\xec\x03\x56\x96\xc8",
    "Are you human?",
    "What languages do you know",
    "Can you talk fast?",
    "My WPM is 420 words by the way. You know that it's great sign of intelligence?",
    "Don't matter if I remember anything tho",
    "Anyway, you are boring, bye",
]
