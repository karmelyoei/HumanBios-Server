from server.states.authorization_state import AccessTokensMenuState  # , NewAccessTokenState
# from server.db import PermissionLevel
from . import handler  # , isstatus
# from copy import deepcopy
import pytest


test_data = (
    ("Identity(2)",   0, 4),
    ("Identity(4)",   2, 2),
    ("Identity(0)",   1, 20),
    ("Identity(12)",  7, 5),
    ("Identity(999)", 24, 15),
)


@pytest.fixture(scope="function")
def access_tokens():
    return [{"identity": f"Identity({i})", "token": f"Token({i})"} for i in range(250)]


@pytest.fixture(scope="function")
def atm_state():
    state = AccessTokensMenuState(
        db=handler.db,
        tr=handler.translator,
        nlu=handler.nlu,
        strings=handler.strings,
        bots_data=handler.bots_data,
    )
    state.set_language("en")
    return state


# Actual tests

"""
@pytest.mark.asyncio
@pytest.mark.parametrize("identity, page, page_size", [
    (""
])
async def test_authentication_state_inputs(atm_state, access_tokens, monkeypatch, user, context):
    context = deepcopy(context)

    # Patched check_websession
    async def patched_query_access_tokens(identity, page, page_size):
        return access_tokens[page*page_size : page*page_size + page_size], len(access_tokens)  # noqa: E203, E226

    # Monkeypatch db object
    monkeypatch.setattr(handler.db, "query_access_tokens", patched_query_access_tokens)

    # User is not an admin, completely ignored.
    user["permissions"] = PermissionLevel.DEFAULT
    context["request"]["message"]["text"] = None

    status = await atm_state.entry(context, user, handler.db)
    assert isstatus(status)
    assert context["request"]["message"]["text"] is None
"""


@pytest.mark.asyncio
@pytest.mark.parametrize("identity, page, page_size", test_data)
async def test_access_tokens_menu_state_get_page_method_empty_data(atm_state, monkeypatch, user, identity, page, page_size):
    # Patched check_websession
    async def patched_qat_empty(identity, page, page_size):
        return [], 0

    # Monkeypatch db object
    monkeypatch.setattr(handler.db, "query_access_tokens", patched_qat_empty)

    user["context"]["tokens_menu"] = {
        "current_page": {},
    }
    text, total, buttons = await atm_state.get_page(user, identity, page, page_size)

    assert text is None
    assert total == 0


@pytest.mark.asyncio
@pytest.mark.parametrize("identity, page, page_size", test_data)
async def test_access_tokens_menu_state_get_page_method_with_data(atm_state, access_tokens, monkeypatch, user, identity, page, page_size):
    # Patched check_websession
    async def patched_query_access_tokens(identity, page, page_size):
        return access_tokens[page*page_size : page*page_size + page_size], len(access_tokens)  # noqa: E203, E226

    # Monkeypatch db object
    monkeypatch.setattr(handler.db, "query_access_tokens", patched_query_access_tokens)

    user["context"]["tokens_menu"] = {
        "current_page": {},
    }
    text, total, buttons = await atm_state.get_page(user, identity, page, page_size)

    if text:
        assert text.key == "authorized_list_token"
        assert page * page_size < total
    else:
        assert page * page_size > total
    assert total == 250


""" This needs to use wrapped_entry for proper testing, because permission check happens there. But it will require some more mocking
@pytest.mark.asyncio
async def test_access_tokens_menu_state_bad_permission(atm_state, access_tokens, monkeypatch, user, context):
    context = deepcopy(context)

    # Patched check_websession
    async def patched_query_access_tokens(identity, page, page_size):
        return access_tokens[page*page_size : page*page_size + page_size], len(access_tokens)  # noqa: E203, E226

    # Monkeypatch db object
    monkeypatch.setattr(handler.db, "query_access_tokens", patched_query_access_tokens)

    # User is not an admin, completely ignored.
    user["permissions"] = PermissionLevel.DEFAULT
    context["request"]["message"]["text"] = None

    status = await atm_state.wrapped_entry(context, user, handler.db)
    assert isstatus(status)
    assert context["request"]["message"]["text"] is None


@pytest.mark.asyncio
async def test_access_tokens_menu_state_entry(atm_state, access_tokens, monkeypatch, context, user):
    context = deepcopy(context)

    # Patched check_websession
    async def patched_query_access_tokens(identity, page, page_size):
        return access_tokens[page*page_size : page*page_size + page_size], len(access_tokens)  # noqa: E203, E226

    # Monkeypatch db object
    monkeypatch.setattr(handler.db, "query_access_tokens", patched_query_access_tokens)

    # Elevate priviledges for this user.
    user["permissions"] = PermissionLevel.ADMIN
    context["request"]["message"]["text"] = "/command"

    status = await atm_state.wrapped_entry(context, user, handler.db)
    assert isstatus(status)

    assert context["request"]["message"]["text"].key == "authorized_list_tokens"
    assert len(context["request"]["buttons"]) == 4
    assert len(context["request"]["buttons"][0]) == 3
    assert len(context["request"]["buttons"][1]) == 3
    assert len(context["request"]["buttons"][2]) == 3

    assert len(context["request"]["buttons"][3]) == 3
    assert context["request"]["buttons"][3][0]["text"].key == "previous"
    assert context["request"]["buttons"][3][1]["text"].key == "back"
    assert context["request"]["buttons"][3][2]["text"].key == "next"
"""
