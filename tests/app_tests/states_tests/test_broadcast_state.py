from server.states.broadcast_feature import BroadcastingState
from typing import List, Optional, Dict, Tuple, Any
from . import handler
import pytest


test_data: List[Tuple[str, Optional[Dict[str, List[Any]]]]] = [
    # Single line
    ("", {}),
    (" ", None),
    ("56012941", None),
    ("=1", {"=": [False, "=", 1]}),
    ("!=100", {"!=": [True, "=", 100]}),
    (">951ck4", {">": [False, ">", "951ck4"]}),
    ("<9991", {"<": [False, "<", 9991]}),
    ("}aaaaaa", {"}": [False, "}", "aaaaaa"]}),
    ("!}id69420", {"!}": [True, "}", "id69420"]}),
    ("!{[1095329, 91593134]", {"!{": [True, "{", "[1095329, 91593134]"]}),
    ("!=[9, 7, 5, a3, a1, 0]", {"!=": [True, "=", "[9, 7, 5, a3, a1, 0]"]}),
    # Multiple lines
    (">90184\n<90100", {">": [False, ">", 90184], "<": [False, "<", 90100]}),
    ("!=9232\n{[2,4,6,8,10]", {"!=": [True, "=", 9232], "{": [False, "{", "[2,4,6,8,10]"]}),
    # More tests needed
]


@pytest.fixture(scope="module")
def bc_state():
    # Subclass the state to use private methods normally
    class TestBroadcastingState(BroadcastingState):
        def parse_targets(self, message):
            return self._parse_targets(message)

        def get_targets(self, parsed_targets, db):
            return self._get_targets(parsed_targets, db)

    return TestBroadcastingState(
        db=handler.db,
        tr=handler.translator,
        nlu=handler.nlu,
        strings=handler.strings,
        bots_data=handler.bots_data,
    )


# Actual tests


@pytest.mark.parametrize("text, expected", test_data)
def test_broadcasting_state_parse_targets_method(bc_state, text, expected):
    parsed = bc_state.parse_targets(text)

    assert parsed == expected
