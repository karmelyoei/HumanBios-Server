from server.settings import TEST_ALLOW_CPU_LOAD, TEST_ALLOW_NETWORK_LOAD
from . import texts, ai_state
from random import sample
import asyncio
import inspect
import pytest


def sample_3(texts, k=5):
    return zip(sample(texts, k=k), sample(texts, k=k), sample(texts, k=k))


# Actual tests


@pytest.mark.dependency(name="attr_get_response")
def test_make_sure_get_response_exists():
    # Make sure there is such attribute
    assert hasattr(ai_state, "get_response")
    # Make sure it is a coroutine
    assert inspect.iscoroutinefunction(ai_state.get_response)


@pytest.mark.asyncio
@pytest.mark.dependency(depends=["attr_get_response"])
@pytest.mark.skipif(not(TEST_ALLOW_NETWORK_LOAD), reason="Do not download model without consent")
@pytest.mark.skipif(not(TEST_ALLOW_CPU_LOAD), reason="Might be brutal for the tester to run NN")
@pytest.mark.parametrize("text", sample(texts, k=10))
async def test_ai_state_get_response_method_single_user(text):
    identity = "uniq-#15v1cm150m0513"
    answer = await ai_state.get_response(identity, text)

    assert answer
    assert type(answer) == str


@pytest.mark.asyncio
@pytest.mark.dependency(depends=["attr_get_response"])
@pytest.mark.skipif(not(TEST_ALLOW_NETWORK_LOAD), reason="Do not download model without consent")
@pytest.mark.skipif(not(TEST_ALLOW_CPU_LOAD), reason="Might be brutal for the tester to run NN")
@pytest.mark.parametrize("text1, text2, text3", sample_3(texts))
async def test_ai_state_get_response_method_multiple_users(text1, text2, text3):
    result1, result2, result3 = await asyncio.gather(*[
        ai_state.get_response("uniq-text-1-identity#jnk4c1", text1),
        ai_state.get_response("uniq-text-2-identity#984n12", text2),
        ai_state.get_response("uniq-text-3-identity#e12cks", text3),
    ])

    assert result1
    assert type(result1) == str

    assert result2
    assert type(result2) == str

    assert result3
    assert type(result3) == str
