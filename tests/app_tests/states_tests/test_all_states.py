from . import isstatus, states, texts, handler
from copy import deepcopy
import itertools
import pytest


@pytest.fixture
def merged_data(users):
    result = list()
    for data in itertools.product(users, texts):
        result.append(data)
    return result


@pytest.mark.asyncio
@pytest.mark.parametrize("state_name, state", states)
async def test_all_states_entry_return_status_codes(state_name, state, merged_data, context, db):
    ctx = deepcopy(context)
    assert ctx

    assert state_name

    for user, text in merged_data:
        ctx["request"]["message"]["text"] = text
        state_obj = state(
            db=handler.db,
            tr=handler.translator,
            nlu=handler.nlu,
            strings=handler.strings,
            bots_data=handler.bots_data,
        )

        # check if user has language
        assert "language" in user
        state_obj.set_language(user["language"])

        status = await state_obj.entry(ctx, user, db)
        assert isstatus(status)


@pytest.mark.asyncio
@pytest.mark.parametrize("state_name, state", states)
async def test_all_states_process_returns_status_codes(state_name, state, users, context, db):
    ctx = deepcopy(context)
    assert ctx

    assert state_name

    state_obj = state(
        db=handler.db,
        tr=handler.translator,
        nlu=handler.nlu,
        strings=handler.strings,
        bots_data=handler.bots_data,
    )

    for user in users:
        assert "language" in user
        state_obj.set_language(user["language"])

        status = await state_obj.entry(ctx, user, db)
        assert isstatus(status)

        for text in texts:
            ctx["request"]["message"]["text"] = text
            status = await state_obj.process(ctx, user, db)
            assert isstatus(status)
