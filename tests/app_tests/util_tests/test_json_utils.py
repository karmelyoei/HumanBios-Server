from typing import List, Union, Tuple, Type
from server.strings import TextPromise
from decimal import Decimal
import server.utils
import pytest


data_and_new_type: List[Union[Tuple[Decimal, Type[float]], Tuple[TextPromise, Type[str]]]] = [
    (Decimal(0), float),
    (Decimal(10000), float),
    (Decimal(-999), float),
]
for i in range(1, 4):
    tp = TextPromise("en", f"key-#{i}")
    tp.fill(" ".join("value" for _ in range(i)))
    data_and_new_type.append((tp, str))


# Actual tests


@pytest.mark.dependency(name="attr_custom_encoder")
def test_has_custom_encoder_for_json():
    assert hasattr(server.utils, "CustomEncoder")


@pytest.mark.dependency(name="attr_custom_default")
def test_has_custom_default_for_json():
    assert hasattr(server.utils, "custom_default")


@pytest.mark.dependency(depends=["attr_custom_encoder"])
@pytest.mark.parametrize("value, expected_type", data_and_new_type)
def test_custom_encoder_class_transform_values(value, expected_type):
    # Prepare encoder class
    ce = server.utils.CustomEncoder()

    assert ce.default(value) == expected_type(value)


@pytest.mark.dependency(depends=["attr_custom_default"])
@pytest.mark.parametrize("value, expected_type", data_and_new_type)
def test_custom_default_function_transform_values(value, expected_type):
    assert server.utils.custom_default(value) == expected_type(value)
