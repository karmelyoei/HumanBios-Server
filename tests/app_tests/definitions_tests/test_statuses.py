from server.definitions.status import BaseStatus
import server.definitions
import pytest


@pytest.mark.dependency(name="attr_statuses")
@pytest.mark.parametrize("status", ["OK", "END", "BACK", "GO_TO_STATE"])
def test_all_required_statuses_exist(status):
    assert hasattr(server.definitions, status)


@pytest.mark.dependency(depends=["attr_statuses"])
@pytest.mark.parametrize("status", ["OK", "END", "BACK", "GO_TO_STATE"])
def test_all_statuses_inherit_from_base(status):
    status_class = getattr(server.definitions, status)

    assert issubclass(status_class, BaseStatus)
