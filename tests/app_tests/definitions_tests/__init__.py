from copy import deepcopy


def copy(*args):
    res = [deepcopy(arg) for arg in args]
    return res if len(res) > 1 else res[0]
