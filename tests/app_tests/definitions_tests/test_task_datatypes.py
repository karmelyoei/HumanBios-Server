import server.definitions
import pytest


@pytest.mark.dependency(name="attr_dtypes")
@pytest.mark.parametrize("dtype", ["SenderTask", "ExecutionTask"])
def test_contains_required_task_datatypes(dtype):
    assert hasattr(server.definitions, dtype)


@pytest.mark.dependency(depends=["attr_dtypes"])
@pytest.mark.parametrize("dtype, attributes", [
    ("SenderTask", ["service", "context"]),
    ("ExecutionTask", ["func", "args", "kwargs"])
])
def test_contains_required_according_task_attributes(dtype, attributes):
    datatype = getattr(server.definitions, dtype)

    for attr in attributes:
        assert hasattr(datatype, attr)
