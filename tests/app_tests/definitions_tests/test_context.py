from server.settings import SERVER_SECURITY_TOKEN
from server.definitions import Context
from . import copy
import pytest


bad_data = [
    {"nothing": "empty"},
    {
        "security_token": "XXX",
        "via_instance": "UNIQUE_ID",
        "service_in": "WhatsApp",
        "service_out": "Facebook",
    },
    {
        "user": {"user_id": 10000000},
        "chat": {"chat_id": 99999999}
    }
]


# Actual tests


@pytest.mark.dependency(name="attr_context")
def test_context_has_expected_values(source_data):
    result = Context.from_json(source_data)

    assert hasattr(result, "validated")
    assert hasattr(result, "object")
    assert hasattr(result, "error")


@pytest.mark.dependency(name="ctx_from_json", depends=["attr_context"])
def test_context_from_json_success(source_data):
    result = Context.from_json(source_data)

    assert result.validated is True


@pytest.mark.dependency(name="ctx_returns_obj", depends=["attr_context", "ctx_from_json"])
def test_context_from_json_success_and_returned_object(source_data, parsed_data):
    result = Context.from_json(source_data)
    assert result.validated is True

    obj = result.object
    # Compare representation as strings, because different types
    assert str(obj) == str(parsed_data)


@pytest.mark.dependency(depends=["attr_context", "ctx_from_json", "ctx_returns_obj"])
def test_context_replace_security_token_method(raw):
    # Make sure we don't have token
    assert raw["security_token"] != SERVER_SECURITY_TOKEN
    result = Context.from_json(copy(raw))
    assert result.validated is True

    obj = result.object
    # Make sure security_token is what we expect
    assert obj["request"]["security_token"] != SERVER_SECURITY_TOKEN
    assert obj["request"]["security_token"] == raw["security_token"]

    # Replace the token and make sure it is what we expect
    obj.replace_security_token()
    assert obj["request"]["security_token"] == SERVER_SECURITY_TOKEN
    assert obj["request"]["security_token"] != raw["security_token"]


@pytest.mark.dependency(depends=["attr_context", "ctx_from_json"])
@pytest.mark.parametrize("data", bad_data)
def test_context_from_json_unvalidated_on_bad_data(data):
    result = Context.from_json(data)

    assert result.validated is False


# Getting correct source but wrapped into weird types


@pytest.mark.dependency(depends=["attr_context", "ctx_from_json"])
def test_context_from_json_unvalidated_string_type(source_data):
    result = Context.from_json(str(source_data))

    assert result.validated is False


@pytest.mark.dependency(depends=["attr_context", "ctx_from_json"])
def test_context_from_json_unvalidated_tuple_type(source_data):
    result = Context.from_json((source_data,))

    assert result.validated is False
