from server.utils.parser import parse_api, get_text
import pytest


# TODO: maybe some default (e.g. expected_types) might be an enum
#       at some point, then we can make sure it is one of the values
default_values = {
    "id": bool,  # make sure it's not "" or None
    "type": lambda o: o and type(o) == str,
    "is_left_side": lambda o: type(o) == bool,
    "buttons": lambda o: type(o) == list,
    "free_answer": lambda o: type(o) == bool,
    "multichoice": lambda o: type(o) == bool,
    "text_key": lambda o: o and type(o) == str,
    "commands": lambda o: type(o) == list,
    "command_args": lambda o: type(o) == dict,
    "expected_type": lambda o: o and type(o) == str,
    "text": lambda o: type(o) == str,
    "is_first_message": lambda o: type(o) == bool,
}
expected_keys = default_values.keys()
raw_data = [
    {
        "id": "1",
        "is_left_side": True,
        "text": "First Message.",
        "type": "text",
        "next_message": "2",
        "is_first_message": True,
    },
    {
        "id": "2",
        "is_left_side": True,
        "text": None,
        "type": "quickreplies",
        "next_message": None,
        "is_first_message:": False,
        "quickreplies": [
            {"text": "text1", "next_message": "3"},
            {"text": "text2", "next_message": "3"},
        ],
    },
    {
        "id": "3",
        "is_left_side": True,
        "text": "Message with command: #special",
        "type": "text",
        "next_message": "4",
        "is_first_message": False,
        "buttons": [
            {"text": "textX", "next_message": "4"},
        ],
    },
    {
        "id": "4",
        "is_left_side": False,
        "text": "Some User Input",
        "type": "text",
        "next_message": "41",
        "is_first_message": False,
    },
    {
        "id": "41",
        "is_left_side": True,
        "text": "My special buttons go next #special_answer=score",
        "type": "text",
        "next_message": "42",
        "is_first_message": False,
    },
    {
        "id": "42",
        "is_left_side": True,
        "text": None,
        "type": "quickreplies",
        "next_message": None,
        "is_first_message": False,
        "quickreplies": [
            {"text": "answer1 #points=10", "next_message": None},
            {"text": "answer2 #points=0", "next_message": None},
            {"text": "answer3 #points=-2", "next_message": None},
        ],
    },
]
parsed_data = parse_api(raw_data)
parsed_ids = [msg["id"] for msg in parsed_data]
all_parsed_btns = list()
for msg in parsed_data:
    all_parsed_btns.extend(msg.get("buttons", []))
all_unique_objects = parsed_data + all_parsed_btns
command_parser_values = [
    (
        "",
        (
            "",
            False,
            [],
            {},
        )
    ),
    (
        "test",
        (
            "test",
            False,
            [],
            {},
        )
    ),
    (
        "command #test",
        (
            "command",
            True,
            ["test"],
            {},
        )
    ),
    (
        "multiple commands #ai #warning #record",
        (
            "multiple commands",
            True,
            ["ai", "warning", "record"],
            {},
        )
    ),
    (
        "command with value #s=60",
        (
            "command with value",
            True,
            ["s"],
            {"s": "60"},
        )
    ),
    (
        "one command multiple values #task=add;arg1=2;arg2=81",
        (
            "one command multiple values",
            True,
            ["task"],
            {"task": "add", "arg1": "2", "arg2": "81"}
        )
    ),
    (
        "bunch of everything #stop #task=do_x;x=something #set_mode=play",
        (
            "bunch of everything",
            True,
            ["stop", "task", "set_mode"],
            {"task": "do_x", "x": "something", "set_mode": "play"}
        )
    )
]


@pytest.fixture
def get_all_unique_objects():
    return all_unique_objects


# Actual tests


@pytest.mark.parametrize("text, expected", command_parser_values)
def test_get_text_parsing_function(text, expected):
    # Parse text
    result = get_text(text)

    assert result == expected


@pytest.mark.dependency(name="attr_parsed_api")
@pytest.mark.parametrize("parsed_msg", parsed_data)
@pytest.mark.parametrize("expected_key", expected_keys)
def test_all_messages_have_expected_keys(parsed_msg, expected_key):
    # Check if the key is there
    assert expected_key in parsed_msg


@pytest.mark.dependency(depends=["attr_parsed_api"])
@pytest.mark.parametrize("parsed_msg", parsed_data)
@pytest.mark.parametrize("key, check", default_values.items())
def test_all_messages_have_expected_keywords_value_and_type(parsed_msg, key, check):
    # Check if the default value is what expected
    assert check(parsed_msg[key])


@pytest.mark.dependency(depends=["attr_parsed_api"])
@pytest.mark.parametrize("raw_msg", raw_data)
def test_all_messages_have_correctly_been_parsed(raw_msg):
    # Check if second message (type = quickreplies) is merged
    if raw_msg["type"] == "quickreplies":
        assert raw_msg["id"] not in parsed_ids
    # Check if user input was parsed correctly
    if not raw_msg["is_left_side"]:
        assert raw_msg["id"] not in parsed_ids
    # TODO: anything else?


@pytest.mark.dependency(name="attr_btns_pass", depends=["attr_parsed_api"])
@pytest.mark.parametrize("btn", all_parsed_btns)
def test_added_keys_to_the_buttons(btn):
    assert "text_key" in btn


@pytest.mark.dependency(depends=["attr_parsed_api", "attr_btns_pass"])
def test_all_keys_are_unique(get_all_unique_objects):
    # Collect all unique keys
    cache_list = list()
    for obj in get_all_unique_objects:
        cache_list.append(obj["text_key"])
    # Post-processing
    cache_set = set(cache_list)

    # Check for unique-ness
    assert len(cache_list) == len(cache_set)
