from server.utils import save_file
import pytest
import shutil
import time
import json
import os


# All data to write
files_data = [
    {},
    # Generate many multiple-value files
    *({f"key-#{n}": f"value-#{n}" for n in range(i)} for i in range(10)),
]


@pytest.fixture(scope="module")
def serve_temp_dir_for_save_file():
    tmp_path = "tmp_save_file"
    yield tmp_path
    shutil.rmtree(tmp_path)


# Actual tests


@pytest.mark.parametrize("data", files_data)
def test_save_file_parser_function_writes_to_latest(data, serve_temp_dir_for_save_file):
    # Write the file
    save_file(data, serve_temp_dir_for_save_file)
    # Read new file
    with open(os.path.join(serve_temp_dir_for_save_file, "latest.json")) as latest:
        written_data = json.load(latest)

    assert written_data == data


@pytest.mark.parametrize("data", files_data)
def test_save_file_parser_function_dumps_up_to_5_files(data, serve_temp_dir_for_save_file):
    # Write the file
    save_file(data, serve_temp_dir_for_save_file)
    # Read file list
    history = os.listdir(serve_temp_dir_for_save_file)

    # Total length is 5 or less
    assert len(history) <= 5
    # Total length without "latest.json" is 4 or less
    modified_h = history[:]
    modified_h.remove("latest.json")
    assert len(modified_h) <= 4
    # Add sleep between the tests
    #    Reason: We save files with timestamps keeping 1 second precision, so to ensure
    #            that old files are not rewritten on top of each other, which is different
    #            to real environment, we want to arbitrary slow down this test.
    time.sleep(1.1)
