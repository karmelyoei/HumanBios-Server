from typing import List, Any


bad_data: List[Any] = [
    None,
    {},
    [],
    "",
    "root",
    "exit()",
    124840129,
    0o142132,
    0b110101101101011111,
    ("break", "into", "parser"),
]
