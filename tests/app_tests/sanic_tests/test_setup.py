from server.__main__ import app
from . import bad_data
import secrets
import pytest
import json


@pytest.mark.parametrize("data", bad_data)
def test_sending_different_bad_data(data):
    request, response = app.test_client.post("/api/setup", data=json.dumps(data))
    data = response.json

    assert data["status"] == 403
    assert data["message"] == "expected json"


def test_sending_no_token(req_data):
    del req_data["security_token"]
    request, response = app.test_client.post("/api/setup", data=json.dumps(req_data))
    data = response.json

    assert data["status"] == 403
    assert data["message"] == "token unauthorized"


def test_sending_empty_token(req_data):
    req_data["security_token"] = ""
    request, response = app.test_client.post("/api/setup", data=json.dumps(req_data))
    data = response.json

    assert data["status"] == 403
    assert data["message"] == "token unauthorized"


def test_sending_no_url(req_data):
    del req_data["url"]
    request, response = app.test_client.post("/api/setup", data=json.dumps(req_data))
    data = response.json

    assert data["status"] == 403
    assert data["message"] == "url invalid"


def test_sending_empty_url(req_data):
    req_data["url"] = ""
    request, response = app.test_client.post("/api/setup", data=json.dumps(req_data))
    data = response.json

    assert data["status"] == 403
    assert data["message"] == "url invalid"


def test_sending_bad_broadcast(req_data):
    req_data["broadcast"] = ""
    req_data["url"] = "http://test.com" + secrets.token_urlsafe(16)
    request, response = app.test_client.post("/api/setup", data=json.dumps(req_data))
    data = response.json

    assert data["status"] == 403
    assert data["message"] == "broadcast entity is invalid (for 'no entity' value must be None)"


def test_sending_bad_psychological_room(req_data):
    req_data["psychological_room"] = ""
    req_data["url"] = "http://test.com" + secrets.token_urlsafe(16)
    request, response = app.test_client.post("/api/setup", data=json.dumps(req_data))
    data = response.json

    assert data["status"] == 403
    assert data["message"] == "psychological room is invalid (for 'no entity' value must be None)"


def test_sending_bad_doctor_room(req_data):
    req_data["doctor_room"] = ""
    req_data["url"] = "http://test.com" + secrets.token_urlsafe(16)
    request, response = app.test_client.post("/api/setup", data=json.dumps(req_data))
    data = response.json

    assert data["status"] == 403
    assert data["message"] == "doctor room is invalid (for 'no entity' value must be None)"
