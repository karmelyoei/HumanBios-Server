from server.db import PermissionLevel
from tidylib import tidy_document
from server.__main__ import app
from itertools import product
from typing import List, Any
from copy import deepcopy
import pytest


user_ids = [
    "user-id-normal",
    "user-id-broadcaster",
    "user-id-admin",
]
tokens = [
    "token-1-2-3-4-5",
    "uniq-token-for-bc",
    "this_little_admin",
]
urls = [
    "/admin",
    "/admin/permissions",
    "/admin/edit_creds",
    # generate tokens with urls
    *(f"/admin/auth/{token}" for token in tokens),
]
cookies: List[Any] = [
    None,
    {},
    *({"session": user_id} for user_id in user_ids)
]


@pytest.fixture
def different_users(user):
    normal = deepcopy(user)
    normal["user_id"] = user_ids[0]
    normal["entity"] = "user-1-identity-uniq"
    normal["permissions"] = PermissionLevel.DEFAULT
    broadcaster = deepcopy(user)
    broadcaster["user_id"] = user_ids[1]
    broadcaster["entity"] = "user-2-identity-uniq"
    broadcaster["permissions"] = PermissionLevel.ADMIN
    admin = deepcopy(user)
    admin["user_id"] = user_ids[2]
    admin["entity"] = "user-3-identity-uniq"
    admin["permissions"] = PermissionLevel.ADMIN
    return {
        normal["user_id"]: normal,
        broadcaster["user_id"]: broadcaster,
        admin["user_id"]: admin,
    }, {
        normal["identity"]: normal,
        broadcaster["identity"]: broadcaster,
        admin["identity"]: admin,
    }, {
        tokens[0]: normal,
        tokens[1]: broadcaster,
        tokens[2]: admin,
    }


@pytest.fixture
def reqs_db(db, different_users, monkeypatch):
    user_id_users, identity_users, tokens_users = different_users

    # Patched check_websession
    async def patched_cw(session_id):
        if session_id:
            assert type(session_id) == str
            return True, user_id_users[session_id]["identity"]

    # Patched get_user
    async def patched_gu(identity):
        if identity:
            assert type(identity) == str
            return identity_users[identity]

    # Patched scan_users
    async def patched_su(attr, value, conf):
        for entry in (user_id_users[user_ids[1]], user_id_users[user_ids[2]]):
            yield entry

    # Patched check_webtoken
    async def patched_cwt(token):
        if token:
            assert type(token) == str
            return tokens_users[token]["identity"]

    # Patched create_websession
    async def patched_cws(identity):
        if identity:
            assert type(identity) == str
            return identity_users[identity]["user_id"]

    # Monkeypatch db object
    monkeypatch.setattr(db, "check_websession", patched_cw)
    monkeypatch.setattr(db, "get_user", patched_gu)
    monkeypatch.setattr(db, "scan_users", patched_su)
    monkeypatch.setattr(db, "check_webtoken", patched_cwt)
    monkeypatch.setattr(db, "create_websession", patched_cws)
    # Return modified db object
    return db


# Actual tests


@pytest.mark.parametrize("url, cookie", product(urls, cookies))
def test_get_html_page_from_urls_with_some_cookies(url, cookie, reqs_db):
    # Make get request
    request, response = app.test_client.get(url, cookies=cookie)
    # Parse document
    doc, errors = tidy_document(response.body)

    assert doc
    assert errors == ""
