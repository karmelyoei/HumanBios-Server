from server.__main__ import app
from . import bad_data
import pytest
import json


@pytest.mark.parametrize("data", bad_data)
def test_process_message_sending_different_bad_data(data):
    request, response = app.test_client.post("/api/process_message", data=json.dumps(data))
    data = response.json

    assert data["status"] == 403
    assert data["message"] == "expected json"


def test_process_message_send_bad_auth(source_data):
    request, response = app.test_client.post("/api/process_message", data=json.dumps(source_data))
    data = response.json

    assert data["status"] == 403
    assert data["message"] == "instance is not registered"


def test_process_message_send_good_auth_and_bad_arg_service_in(raw):
    # Prepare data
    del raw["service_in"]
    # Make request
    request, response = app.test_client.post("/api/process_message", data=json.dumps(raw))
    data = response.json
    # Perform check
    assert data["status"] == 403
    assert data["message"] == "'service_in' is a required property"


def test_process_message_send_good_auth_and_bad_arg_user(raw):
    # Prepare data
    del raw["user"]
    # Make request
    request, response = app.test_client.post("/api/process_message", data=json.dumps(raw))
    data = response.json
    # Perform check
    assert data["status"] == 403
    assert data["message"] == "'user' is a required property"


def test_process_message_send_good_auth_and_bad_arg_user_id(raw):
    # Prepare data
    del raw["user"]["user_id"]
    # Make request
    request, response = app.test_client.post("/api/process_message", data=json.dumps(raw))
    data = response.json
    # Perform check
    assert data["status"] == 403
    assert data["message"] == "'user_id' is a required property"


def test_process_message_send_good_auth_and_bad_arg_chat(raw):
    # Prepare data
    del raw["chat"]
    # Make request
    request, response = app.test_client.post("/api/process_message", data=json.dumps(raw))
    data = response.json
    # Perform check
    assert data["status"] == 403
    assert data["message"] == "'chat' is a required property"


def test_process_message_send_good_auth_and_bad_arg_chat_id(raw):
    # Prepare data
    del raw["chat"]["chat_id"]
    # Make request
    request, response = app.test_client.post("/api/process_message", data=json.dumps(raw))
    data = response.json
    # Perform check
    assert data["status"] == 403
    assert data["message"] == "'chat_id' is a required property"


@pytest.mark.skip(reason="Try to find a prod CI bug")
def test_process_message_send_good_auth_and_good_minimum_data(raw):
    # Make request
    request, response = app.test_client.post("/api/process_message", data=json.dumps(raw))
    data = response.json
    # Perform check
    assert data["status"] == 200
    assert data == {"status": 200}


@pytest.mark.skip(reason="Try to find a prod CI bug")
def test_process_message_send_good_auth_and_good_data_and_additional_data(raw):
    # Prepare data
    raw["file"] = [{"payload": "http://image.com"}]
    raw["has_file"] = True
    raw["has_image"] = True
    # Make request
    request, response = app.test_client.post("/api/process_message", data=json.dumps(raw))
    data = response.json
    # Perform check
    assert data["status"] == 200
    assert data == {"status": 200}


@pytest.mark.skip(reason="Try to find a prod CI bug")
def test_process_message_send_good_auth_and_good_data(raw):
    # Prepare data
    raw["message"] = {}
    raw["message"]["text"] = "/start"
    raw["file"] = [
        {"payload": "http://image1.com"},
        {"payload": "http://image2.com"}
    ]
    raw["has_file"] = True
    raw["has_image"] = True
    # Make request
    request, response = app.test_client.post("/api/process_message", data=json.dumps(raw))
    data = response.json
    # Perform check
    assert data["status"] == 200
    assert data == {"status": 200}
