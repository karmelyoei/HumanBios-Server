from server.db import Database, AccountType, PermissionLevel, User
from boto3.dynamodb.conditions import Attr
from typing import Generator
from random import choice
import pytest
import uuid


db = Database()
services    = ["telegram", "facebook", "web-chat", "web-widget"]
instances   = ["telegram-1", "telegram-2", "facebook-1", "web-chat-1"]
first_names = [None, "Angel", "Hether", "Peter", "Josh", "kyle"]
last_names  = [None, "Patterson", "person", "hooman", "x", "anonymous"]
usernames = [
    "anonymous", "cool_1231", "snaek1", "x___x", "jotimagine",
    "pagejot", "icepitter", "lifehuh", "mindgiggle", "youngswoosh",
]
languages = ["en", "ru", "ua", "de", "he", "es", "fr"]
states_history = [
    ["ENDState"],
    ["ENDState", "StartState", "QAState"],
    ["ENDState", "StartState", "QAState", "AIState"],
]
account_types = [
    AccountType.COMMON,
    AccountType.MEDIC,
    AccountType.SOCIAL,
]
permission_levels = [
    PermissionLevel.ANON,
    PermissionLevel.DEFAULT,
    PermissionLevel.ADMIN,
]


def generate_users(n: int) -> Generator[User, None, None]:
    for _ in range(n):
        yield {
            "user_id":      str(uuid.uuid4()),
            "service":      choice(services),
            "identity":     str(uuid.uuid4()),
            "via_instance": choice(instances),
            "first_name":   choice(first_names),
            "last_name":    choice(last_names),
            "username":     choice(usernames),
            "language":     choice(languages),
            "account_type": choice(account_types),
            "created_at":   db.now().isoformat(),
            "location":     None,
            "last_active":  db.now().isoformat(),
            "conversation": None,
            "answers":      dict(),
            "files":        dict(),
            "states":       choice(states_history),
            "permissions":  choice(permission_levels),
            "context":      dict(),
        }


users = list(generate_users(5))
# Add non-default permission user to avoid theoretical random fail
# when fetching all non-default users from db
for new_user in generate_users(1):
    new_user["permissions"] = PermissionLevel.ADMIN
    users.append(new_user)


# Actual tests


@pytest.mark.asyncio
@pytest.mark.parametrize("obj", users)
@pytest.mark.dependency(name="create_users_1")
async def test_database_create_user_method(obj):
    await db.create_user(obj)


@pytest.mark.asyncio
@pytest.mark.parametrize("obj", users)
@pytest.mark.dependency(name="get_user_1", depends=["create_users_1"])
async def test_database_get_user_method(obj):
    result = await db.get_user(obj["identity"])

    assert result == obj


@pytest.mark.asyncio
@pytest.mark.parametrize("obj", users)
@pytest.mark.dependency(name="commit_user_1")
async def test_database_commit_user_method(obj):
    await db.commit_user(obj)


@pytest.mark.asyncio
@pytest.mark.parametrize("obj", users)
@pytest.mark.dependency(depends=["get_user_1", "commit_user_1"])
async def test_database_update_user_method(obj):
    # Get user from db first
    user_upd = await db.get_user(obj["identity"])
    # Update something
    user_upd["context"]["weather"] = "goes brrr"
    # Commit user in the database
    await db.commit_user(user_upd)
    # Get latest version
    user_new = await db.get_user(user_upd["identity"])

    assert user_new == user_upd


@pytest.mark.asyncio
@pytest.mark.dependency(depends=["create_users_1"])
async def test_database_scan_users_with_permission_level_method():
    result = list()
    async for admin in db.scan_users(
        Attr("permissions").gte(PermissionLevel.ADMIN),
        "#identity, #permissions",
        {"#identity": "identity", "#permissions": "permissions"}
    ):
        result.append(admin)

    # Check that all pulled users have correct rights
    assert all([user["permissions"] != PermissionLevel.DEFAULT for user in result])
