from server.db import Database
import pytest
import uuid


db = Database()
idtts = {str(uuid.uuid4()): None for _ in range(5)}


# Actual tests


@pytest.mark.asyncio
@pytest.mark.dependency(name="create_wt_method")
@pytest.mark.parametrize("idtt", idtts)
async def test_database_create_webtoken_method(idtt):
    token = await db.create_webtoken(idtt)

    assert token
    idtts[idtt] = token


@pytest.mark.asyncio
@pytest.mark.dependency(depends=["create_wt_method"])
@pytest.mark.parametrize("idtt", idtts.keys())
async def test_database_check_webtoken_method(idtt):
    token = idtts.get(idtt)
    assert token

    webtoken = await db.check_webtoken(token)
    assert webtoken


@pytest.mark.asyncio
@pytest.mark.dependency(depends=["create_wt_method"])
@pytest.mark.parametrize("idtt", idtts.keys())
async def test_database_check_webtoken_method_with_delay(idtt):
    token = idtts.get(idtt)
    assert token

    # Call expiration of the token
    db._del_token(token)

    # Try checking webtoken again (should be None)
    webtoken = await db.check_webtoken(token)
    assert webtoken is None
