from server.db import Database
from datetime import timedelta
from copy import deepcopy
import pytest


db = Database()
checkbacks = (
    (timedelta(minutes=15), "Test Reminder 1", "one-time", "QAState"),
    (timedelta(minutes=45), "Test Reminder 2", "daily", "StartState"),
    (timedelta(days=12345), "Test Reminder 3", "weekly", "ENDState"),
)


# Actual tests


@pytest.mark.asyncio
@pytest.mark.dependency(name="create_checkback")
@pytest.mark.parametrize("send_in, title, period, state", checkbacks)
async def test_database_create_checkback_method(user, context, send_in, title, period, state):
    result = await db.create_checkback(user, deepcopy(context.__dict__["request"]), send_in, title, period, state)
    assert result is not None


@pytest.mark.asyncio
@pytest.mark.dependency(name="get_checkback", depends=["create_checkback"])
@pytest.mark.parametrize("send_in, title, period, state", checkbacks)
async def test_database_all_checkbacks_in_range_method_query_single(user, context, send_in, title, period, state):
    # For each time, +- 1 minute, so we query each checkback.
    start = (db.now() + send_in) - timedelta(minutes=1)
    end   = (db.now() + send_in) + timedelta(minutes=1)
    items, count = await db.all_checkbacks_in_range(start, end)

    assert count == 1
    assert len(items) == 1

    # TODO: verify object parameters (maybe redundant)


@pytest.mark.asyncio
@pytest.mark.dependency(name="get_checkbacks", depends=["get_checkback", "create_checkback"])
async def test_database_all_checkbacks_in_range_method_query_many():
    # Make sure this corresponds to timedelta that can query two items.
    start = db.now()
    end   = db.now() + timedelta(minutes=50)
    items, count = await db.all_checkbacks_in_range(start, end)

    assert count == 2
    assert len(items) == 2

    # TODO: verify object parameters (maybe redundant)

    # Make sure this corresponds to timedelta that can query three items.
    start = db.now()
    end   = db.now() + timedelta(days=12346)
    items, count = await db.all_checkbacks_in_range(start, end)

    assert count == 3
    assert len(items) == 3

    # TODO: verify object parameters (maybe redundant)


@pytest.mark.asyncio
@pytest.mark.dependency(name="update_checkback", depends=["get_checkbacks"])
async def test_database_update_checkback_method():
    # Make sure this corresponds to timedelta that can query three items.
    items, _ = await db.all_checkbacks_in_range(db.now(), db.now() + timedelta(days=12346))

    ids = (111222333, 111222333, 444555666)
    for item, chat_id in zip(items, ids):
        result = await db.update_checkback(item["id"], "SET chat_id = :v", {":v": chat_id})
        assert result["Attributes"]["chat_id"] == chat_id


@pytest.mark.asyncio
@pytest.mark.dependency(name="checkbacks_by_chat", depends=["update_checkback"])
async def test_database_query_checkbacks_by_chat_method():
    # Using id from the previous test, this will query 2 items.
    chat_id = 111222333
    items, count = await db.query_checkbacks_by_chat(chat_id, 0, 5)

    assert count == 2
    assert len(items) == 2

    # Using id from the previous test, this will query 1 item.
    chat_id = 444555666
    items, count = await db.query_checkbacks_by_chat(chat_id, 0, 10)

    assert count == 1
    assert len(items) == 1


@pytest.mark.asyncio
@pytest.mark.dependency(depends=["checkbacks_by_chat"])
async def test_database_delete_checkback_method():
    # Make sure this corresponds to existing timdeltas, so that can query all items.
    items, count = await db.all_checkbacks_in_range(db.now(), db.now() + timedelta(days=12346))

    assert count == 3
    assert len(items) == 3

    for item in items:
        result = await db.delete_checkback(item["id"])
        assert result is not None

    # Make sure this corresponds to existing timedeltas, so we can try to query all items.
    items, count = await db.all_checkbacks_in_range(db.now(), db.now() + timedelta(days=12346))

    assert count == 0
    assert len(items) == 0
