from server.db import Database


def test_ensure_database_object_is_one_object():
    db1 = Database()
    db2 = Database()
    db10 = Database()

    assert db1 == db2 == db10
    assert id(db1) == id(db2) == id(db10)
