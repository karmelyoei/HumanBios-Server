from server.db import Database
import pytest
import uuid


db = Database()
idtts = {str(uuid.uuid4()): None for _ in range(3)}


# Actual tests


@pytest.mark.asyncio
@pytest.mark.dependency(name="create_websession")
@pytest.mark.parametrize("idtt", idtts.keys())
async def test_database_create_websession_method(idtt):
    session = await db.create_websession(idtt)

    assert session
    idtts[idtt] = session


@pytest.mark.asyncio
@pytest.mark.dependency(name="check_websession", depends=["create_websession"])
@pytest.mark.parametrize("idtt", idtts)
async def test_database_check_websession_method(idtt):
    # Get session
    session = idtts.get(idtt)
    assert session

    valid, identity = await db.check_websession(session)
    assert valid is False
    assert identity == idtt


@pytest.mark.asyncio
@pytest.mark.dependency(depends=["create_websession", "check_websession"])
@pytest.mark.parametrize("idtt", idtts)
async def test_database_set_websession_status_method(idtt):
    # Get session
    session = idtts.get(idtt)
    assert session

    # Set to true (we do this first, because default is False)
    valid = await db.set_websession_status(session, True)
    assert valid is True

    valid, _ = await db.check_websession(session)
    assert valid is True

    # Set to false (this is done afterwards, so we can verify it was reverted)
    valid = await db.set_websession_status(session, False)
    assert valid is False

    valid, _ = await db.check_websession(session)
    assert valid is False
