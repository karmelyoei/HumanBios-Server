from server.db import Database
import secrets
import hashlib
import random
import pytest
import uuid


db = Database()
texts = [
    "Test text 1",
    "Second text",
    f"Third l{'o' * 100}ng text",
    "Forth",
    "5",
    *(secrets.token_urlsafe(16) for _ in range(25))
]
languages = ["de", "es", "en", "ru", "he", "fr", "uk"]


def generate_tr_item(n: int, language=None):
    for _ in range(n):
        language = language or random.choice(languages)
        text = random.choice(texts)
        tr_text = f"{language}({text})"
        yield {
            "language": language,
            "string_key": str(uuid.uuid4()),
            "text": tr_text,
            "content_hash": hashlib.sha256(tr_text.encode()).digest(),
        }


items = list(generate_tr_item(5))
# Ensure some amount of objects with "de"
for o in generate_tr_item(3):
    o["language"] = "de"
    items.append(o)


# Actual tests


@pytest.mark.asyncio
@pytest.mark.dependency(name="create_ctr_method")
@pytest.mark.parametrize("item", items)
async def test_database_create_translation_method(item):
    await db.create_translation(item)


@pytest.mark.asyncio
@pytest.mark.dependency(name="get_tr_method", depends=["create_ctr_method"])
@pytest.mark.parametrize("item", items)
async def test_database_get_translation_method(item):
    result = await db.get_translation(item["language"], item["string_key"])

    assert result == item


@pytest.mark.asyncio
@pytest.mark.dependency(depends=["get_tr_method"])
async def test_database_query_translations_method():
    # Collect all "de" items
    all_de_items = [item for item in items if item["language"] == "de"]
    result = await db.query_translations("de", [item["string_key"] for item in all_de_items])

    assert result == all_de_items
