from server.db import PermissionLevel, AccountType, Database, User
from server.definitions import Context
from server.__main__ import app
from datetime import datetime
from copy import deepcopy
from typing import List
import pytest
import pytz
import json


@pytest.fixture
def db():
    return Database()


@pytest.fixture(scope="function")
def source_data():
    return {
        "security_token": "TEST_TOKEN_123",
        "via_instance": "UNIQUE_ID",
        "service_in": "WhatsApp",
        "user": {"user_id": 1232131},
        "chat": {"chat_id": 2131321},
    }


@pytest.fixture(scope="function")
def parsed_data():
    return {
        "request": {
            "security_token": "TEST_TOKEN_123",
            "via_instance": "UNIQUE_ID",
            "service_in": "WhatsApp",
            "user": {
                "user_id": 1232131,
                "first_name": None,
                "last_name": None,
                "username": None,
                "lang_code": None,
                "identity": "fc17d7aa958dc9fafb593932fb4f57f55fbe5d2964de2b969635690408bf6625",
            },
            "chat": {"chat_id": 2131321, "name": None, "chat_type": None, "username": None},
            "service_out": None,
            "has_forward": False,
            "forward": {"user_id": None, "is_bot": False, "first_name": None, "username": None},
            "has_message": False,
            "message": {"text": None, "message_id": None, "update_id": None},
            "has_file": False,
            "has_audio": False,
            "has_video": False,
            "has_document": False,
            "has_image": False,
            "has_location": False,
            "file": [],
            "has_buttons": False,
            "buttons_type": None,
            "buttons": [],
            "instance_context": {},
            "service_context": {},
            "cache": {},
        }
    }


@pytest.fixture(scope="function")
def context(source_data):
    ctx = Context.from_json(source_data)
    assert ctx.validated
    ctx = ctx.object
    assert ctx
    ctx.replace_security_token()
    return ctx


@pytest.fixture(scope="function")
def user():
    assert hasattr(PermissionLevel, "DEFAULT")
    return {
        "user_id": "unique-id-123",
        "service": "telegram",
        "identity": "super-unique",
        "via_instance": "telegram-1",
        "first_name": "tester",
        "last_name": "rose",
        "username": "rose_tester",
        "language": "en",
        "type": AccountType.COMMON,
        "created_at": datetime.now(tz=pytz.utc).isoformat(),
        "last_location": None,
        "last_active": datetime.now(tz=pytz.utc).isoformat(),
        "conversation_id": None,
        "answers": dict(),
        "files": dict(),
        "states": list("ENDState"),
        "permissions": PermissionLevel.DEFAULT,
        "context": dict(),
    }


@pytest.fixture(scope="function")
def users(user) -> List[User]:
    # First user is base user
    user1 = deepcopy(user)

    user2 = deepcopy(user)
    user2["user_id"]    = "new id 00010"
    user2["service"]    = "facebook"
    user2["identity"]   = "axf1tv1br1n"
    user2["first_name"] = "broadcaster"
    user2["last_name"]  = "tyson"

    assert hasattr(PermissionLevel, "ADMIN")
    user2["permissions"] = PermissionLevel.ADMIN

    user3 = deepcopy(user)
    user3["user_id"]    = "fake id 1111111"
    user3["service"]    = "whatsapp"
    user3["identity"]   = "new cool identity"
    user3["first_name"] = "me"
    user3["last_name"]  = "(that guy)"

    assert hasattr(PermissionLevel, "ADMIN")
    user3["permissions"] = PermissionLevel.ADMIN

    return [user1, user2, user3]


@pytest.fixture(scope="function")
def req_data():
    return {
        "security_token": "TEST_TOKEN_123",
        "url": "https://test.com",
        "broadcast": "id-test-broadcast",
        "psychological_room": "id-test-123456",
        "doctor_room": "id-test-doc-0123",
    }


@pytest.fixture(scope="function")
def registered_data(req_data):
    req, resp = app.test_client.post("/api/setup", data=json.dumps(req_data))
    data = resp.json

    assert data["status"] == 200
    return data


@pytest.fixture(scope="function")
def raw(registered_data, source_data):
    raw = deepcopy(source_data)
    raw["security_token"] = registered_data["token"]
    raw["via_instance"] = registered_data["name"]
    return raw
